<header class="navbar navbar-expand-md navbar-light d-print-none">
        <div class="container-xl">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu">
            <span class="navbar-toggler-icon"></span>
          </button>
          <h1 class="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3">
            <a href="{{url('area/admin/home')}}">
              <img src="{{asset('dist/img/logoku.png')}}" width="110" height="32" alt="Tabler" class="navbar-brand-image">
            </a>
          </h1>
          <div class="navbar-nav flex-row order-md-last">
            <div class="nav-item dropdown">
              <a href="#" class="nav-link d-flex lh-1 text-reset p-0" data-bs-toggle="dropdown" aria-label="Open user menu">
                <span class="avatar avatar-sm text-white bg-yellow">{{substr(Session::get('username'),0,1)}}</span>
                <div class="d-none d-xl-block ps-2">
                  <div>{{Session::get('username')}}</div>
                  <div class="mt-1 small text-muted">{{Session::get('no_telpon')}}</div>
                </div>
              </a>
              <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <a href="{{url('area/admin/setting')}}" class="dropdown-item">Settings</a>
                <a href="{{url('area/admin/logout')}}" class="dropdown-item">Logout</a>
              </div>
            </div>
          </div>
        </div>
      </header>
      <div class="navbar-expand-md">
        <div class="collapse navbar-collapse" id="navbar-menu">
          <div class="navbar navbar-light">
            <div class="container-xl">
              <ul class="navbar-nav">
                <li class="nav-item {{ $active ?? '' }}">
                  <a class="nav-link" href="{{url('area/admin/home')}}" >
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><polyline points="5 12 3 12 12 3 21 12 19 12" /><path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7" /><path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6" /></svg>
                    </span>
                    <span class="nav-link-title">
                      Beranda
                    </span>
                  </a>
                </li> 
                <li class="nav-item dropdown {{ $active_trx ?? '' }}">
                  <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                       <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M5 21v-16a2 2 0 0 1 2 -2h10a2 2 0 0 1 2 2v16l-3 -2l-2 2l-2 -2l-2 2l-2 -2l-3 2" /><path d="M14 8h-2.5a1.5 1.5 0 0 0 0 3h1a1.5 1.5 0 0 1 0 3h-2.5m2 0v1.5m0 -9v1.5" /></svg>
                    </span>
                    <span class="nav-link-title">
                    Transaksi
                    </span>
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ url('area/admin/transaction') }}">
                      Ambil Cucian
                    </a> 
                    <a class="dropdown-item" href="{{ url('area/admin/selesai') }}">
                      Data Cucian telah diambil
                    </a>
                  </div>
                </li>
                @if (Session::has('LEVEL_USER'))
                  @if(Session::get('LEVEL_USER') == "SU")
                <li class="nav-item dropdown {{ $active_master ?? '' }}">
                  <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M9 4h3l2 2h5a2 2 0 0 1 2 2v7a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2v-9a2 2 0 0 1 2 -2" /><path d="M17 17v2a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2v-9a2 2 0 0 1 2 -2h2" /></svg>
                    </span>
                    <span class="nav-link-title">
                      Master
                    </span>
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ url('area/admin/service_type') }}">
                      Paket Layanan
                    </a>
                    <a class="dropdown-item" href="{{ url('area/admin/type_unit') }}">
                      Jenis Laundry Satuan
                    </a>
                    <a class="dropdown-item" href="{{ url('area/admin/petugas') }}">
                      Data Petugas Laundry
                    </a>
                  </div>
                </li>
                @endif
                @endif
                <li class="nav-item dropdown {{ $active_laporan ?? '' }}">
                  <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown" role="button" aria-expanded="false">
                    <span class="nav-link-icon d-md-none d-lg-inline-block">
                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M9 4h3l2 2h5a2 2 0 0 1 2 2v7a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2v-9a2 2 0 0 1 2 -2" /><path d="M17 17v2a2 2 0 0 1 -2 2h-10a2 2 0 0 1 -2 -2v-9a2 2 0 0 1 2 -2h2" /></svg>
                    </span>
                    <span class="nav-link-title">
                    Laporan
                    </span>
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ url('area/admin/report/daily') }}">
                      Laporan Harian
                    </a> 
                    <a class="dropdown-item" href="{{ url('area/admin/report/monthly') }}">
                    Laporan Bulanan
                    </a>
                    <a class="dropdown-item" href="{{ url('area/admin/report/annual') }}">
                    Laporan Tahunan
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>