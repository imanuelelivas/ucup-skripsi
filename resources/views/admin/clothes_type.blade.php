<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.head')
  </head>
  <body class="antialiased">
    <div class="wrapper">
      @include('layout.header')

      <div class="page-wrapper">
        <div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  LaundryKu
                </div>
                <h2 class="page-title">
                  Data Laundry Satuan
                </h2>
              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="container-xl">
            <div class="row row-cards">
              <div class="col-md-5 col-xl-5">
                <div class="row row-cards">
                  <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                         <h3 class="card-title">Tambah Laundry Satuan</h3>
                      </div>
                      <div class="card-body">
                      @if(Session::has('flash_message'))
                        <div class="alert {{ Session::get('alert') }} alert-dismissible" role="alert">
                            <div class="d-flex">
                            <div>
                            </div>
                            <div>
                                <h4 class="alert-title" id="alert-title">{{ Session::get('flash_title') }}&hellip;</h4>
                                <div class="text-muted" id="alert-body">{{ Session::get('flash_message') }}</div>
                            </div>
                            </div>
                            <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
                        </div>
                      @endif
                      <form class="add-listing-form" action="{{ url('area/admin/act_type_unit') }}" method="post" id="tambah-jenis">
                      @csrf
                        <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Jenis Pakaian</label>
                              <input type="text" class="form-control" name="jenis" id="jenis" placeholder="Ex. Jaket Kulit" autocomplete="off" required />
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Harga</label>
                              <input type="number" class="form-control" name="harga" id="harga" placeholder="Ex. 12000" autocomplete="off" required />
                            </div>
                          </div>
                        </div>
                        <div class="form-footer">
                          <div class="d-flex">
                            <button type="submit" class="btn btn-primary ms-auto">Tambah</button>
                          </div>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- div samping -->
              <div class="col-md-7 col-xl-7">
                <div class="row row-cards">
                  <div class="col-12">
                    <div class="card">
                      <div class="card-header">
                         <h3 class="card-title">Data Laundry Satuan</h3>
                      </div>
                      <div class="card-body">
                        <div class="card-table table-responsive">
                          <table class="table card-table table-vcenter text-nowrap datatable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Items</th>
                                    <th>Harga(/pcs)</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data_item_satuan as $items)
                                <tr>
                                    <td>{{ ($data_item_satuan->currentPage()-1) * $data_item_satuan->perPage() + $loop->index + 1 }}</td>
                                    <td>{{ $items->jenis_pakaian }}</td>
                                    <td>Rp. {{ number_format($items->harga_satuan) }}</td>
                                    <td class="text-end">
                                        <div class="form-selectgroup">
                                            <a href="#" data-kode="{{ $items->id }}" id="deleteUnit">
                                            <label class="form-selectgroup-item">
                                                <span class="form-selectgroup-label">
	                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="4" y1="7" x2="20" y2="7" /><line x1="10" y1="11" x2="10" y2="17" /><line x1="14" y1="11" x2="14" y2="17" /><path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" /><path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /></svg>
                                                </span>
                                            </label>
                                            </a>
                                            <a href="#" data-id="{{ $items->id }}" id="updateUnit">
                                            <label class="form-selectgroup-item">
                                                <span class="form-selectgroup-label">
	                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 15l8.385 -8.415a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3z" /><path d="M16 5l3 3" /><path d="M9 7.07a7.002 7.002 0 0 0 1 13.93a7.002 7.002 0 0 0 6.929 -5.999" /></svg>
                                                </span>
                                            </label>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                        </div>
                        Showing {{($data_item_satuan->currentPage()-1)* $data_item_satuan->perPage()+($data_item_satuan->total() ? 1:0)}} to {{($data_item_satuan->currentPage()-1)*$data_item_satuan->perPage()+count($data_item_satuan)}}  of  {{$data_item_satuan->total()}}  Results
                      </div>
                      <br>
						{{ $data_item_satuan->links() }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('admin.modal') @include('layout.footer')
      </div>
       <!-- modal -->
    <div class="modal modal-blur fade" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Edit Jenis Pakaian</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                    @csrf
                    <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Jenis Pakaian</label>
                              <input type="text" class="form-control" name="jenis" id="jenis_edt" placeholder="Ex. Jaket Kulit" autocomplete="off" required />
                              <input type="hidden" class="form-control" name="id" id="id_edt" required />
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Harga</label>
                              <input type="number" class="form-control" name="harga" id="harga_edt" placeholder="Ex. 12000" autocomplete="off" required />
                            </div>
                          </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                    Cancel
                    </a>
                    <div class="ms-auto">
                        <button type="submit" class="btn btn btn-primary btn-sm" id="btn-edt">Update
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->
    </div>
   
    <script src="{{asset('dist/js/tabler.min.js')}}"></script>
    <script>
        $("body").on("click", "#deleteUnit", function (event) {
            if (confirm('Apakah anda yakin ingin hapus data ini?')) {
                event.preventDefault();
                var token = $('input[name="_token"]').val();
                var id = $(this).data("kode");
                let _url = "{{ url('area/admin/delete_type_unit') }}";
                $.ajax({
                    url: _url,
                    type: "POST",
                    data: {
                        id: id,
                        _token: token
                    },
                    success: function (response) {
                    if (response.status == "00") {
                        $("#message-success").html(response.message);
                        $('#modal-success').modal('show');
                    } else {
                        $("#message-failed").html(response.message);
                        $('#modal-danger').modal('show');
                    }
                    },
                    error: function (response) {
                    alert(response);
                    },
                });
            }
        });
        
        $("body").on("click", "#updateUnit", function (event) {
            event.preventDefault();
            var token = $('input[name="_token"]').val();
            var id = $(this).data("id");
            let _url = "{{ url('area/admin/get_type_unit') }}";
            $.ajax({
                url: _url,
                type: "POST",
                data: {
                    id: id,
                    _token: token
                },
                success: function (response) {
                    if (response.status == "00") {
                        $("#jenis_edt").val(response.jenis);
                            $("#harga_edt").val(response.harga);
                            $("#id_edt").val(response.id);
                            $('#modal_edit').modal('show');
                    } else {
                        $("#message-failed").html(response.message);
                        $('#modal-danger').modal('show');
                    }
                },
                error: function (response) {
                    alert(response);
                },
            });
        });
        
        $("body").on("click", "#btn-edt", function (event) {
            event.preventDefault();
            var token = $('input[name="_token"]').val();
            var id = $("#id_edt").val();
            var names = $("#jenis_edt").val();
            var prices = $("#harga_edt").val();
            if(names == ''){
              alert('Jenis Pakaian harus diisi');
              $('#jenis_edt').focus();
              return false;
            }
            if(prices == ''){
              alert('Harga harus diisi');
              $('#harga_edt').focus();
              return false;
            }
            let _url = "{{ url('area/admin/update_type_unit') }}";
            $.ajax({
                url: _url,
                type: "POST",
                data: {
                    id: id,
                    name: names,
                    price: prices,
                    _token: token
                },
                success: function (response) {
                    if (response.status == "00") {
                        $("#jenis_edt").val("");
                        $("#harga_edt").val("");
                        $("#id_edt").val("");
                        $('#modal_edit').modal('hide');
                        $("#message-success").html(response.message);
                        $('#modal-success').modal('show');
                    } else {
                        $("#jenis_edt").val("");                        
                        $("#harga_edt").val("");
                        $("#id_edt").val("");
                        $('#modal_edit').modal('hide');
                        $("#message-failed").html(response.message);
                        $('#modal-danger').modal('show');
                    }
                },
                error: function (response) {
                    alert(response);
                },
            });
        });
    </script>
  </body>
</html>
