<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.head')
  </head>
  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="page page-center">
      <div class="container-tight py-4">
        <div class="text-center mb-4">
          <a href="{{url('status/tracking')}}"><img src="{{asset('dist/img/logoku.png')}}" height="36" alt=""></a>
        </div>
        <div class="card card-md">
          <div class="card-body text-center py-4 p-sm-5">
            <img src="{{asset('dist/img/search.png')}}" height="150" class="mb-n2" alt="">
            <h1 class="mt-5">Selamat Datang di LaundryKu. Kotor? Kesini Aja!</h1>
            <!-- <p class="text-muted">LaundryKu adalah Aplikasi Laundry </p> -->
          </div>
          <div class="hr-text hr-text-center hr-text-spaceless">checking Laundry Progress</div>
            <div class="card-body">
              <div class="mb-3">
                <label class="form-label">ID Transaksi</label>
                  <input type="text" class="form-control ps-1" autocomplete="off" name="kode_transaksi" placeholder="Ex. 20210425ED32E" id="kode_transaksi" value="">
              </div>
            </div>
            <button type="button" class="btn btn-primary btn-square" id="btn-progress">
                Cek Cucian
              </button>
          </div>
        </div>
        @include('modal_progress')
        <div class="text-center text-muted mt-3">
          LaundryKu, <a href="{{url('login')}}">login</a> to area admin.
        </div>
      </div>
    </div>
    <script src="./dist/js/tabler.min.js"></script>
  </body>
  <script>
    $("body").on("click", "#btn-progress", function (event) {
      event.preventDefault();
      let _url = "{{ url('check_state') }}";
      if($("#kode_transaksi").val() == ''){
        alert('ID Transaksi wajib diisi');
        return false;
      }
      $.ajax({
         url: _url,
         type: "POST",
         data: {
          kode_transaksi: $("#kode_transaksi").val(),
            _token: "{{ csrf_token() }}"
          },
          beforeSend: function() {
            // setting a timeout
            $("#btn-progress").html('Loading<span class="animated-dots"></span>');
          },      
          success: function (response) {
            if (response.status == '00') {
                $("#title").html("Tracking Cucian " + response.data_laundry.kode);
                $("#code").html(response.data_laundry.kode);
                $("#name").html(response.data_laundry.nama);
                $("#phone").html(response.data_laundry.telpon);
                $("#address").html(response.data_laundry.alamat);
                $("#type_laundry").html(response.data_laundry.jenis_cucian);
                $("#estimated").html(response.data_laundry.type_layanan);
                if(response.data_laundry.berat_cucian != null){
                    $("#weight").html(response.data_laundry.berat_cucian+" Kg");
                }else{
                    $("#weight").html("-");
                }
                $("#price").html("Rp "+formatNumber(response.data_laundry.harga));
                $("#trx_date").html(response.data_laundry.tgl_transaksi);
                $("#trx_date_done").html(response.data_laundry.tgl_selesai);
                $("#trx_done").html(response.data_laundry.tgl_selesai);
                $("#status_laundry").html(response.data_laundry.status_cucian);
                $("#status_payment").html(response.data_laundry.status_pembayaran);
                $("#update_date").html(response.data_laundry.tgl_update);
                $("#info_status_c").html(response.data_laundry.status_cucian);
                $("#info_status_p").html(response.data_laundry.status_pembayaran);
                var newRow = response.data_laundry.dts_laudry;
                $("#divSatuan").show();
                $("#dts_laundry > tbody").empty(); 
                $.each(newRow, function (key, value) {
                    $("#dts_laundry > tbody").append("<tr><td>("+value.qty+") "+value.item+"</td></tr>");
                });
                if (newRow.length === 0) { 
                    $("#divSatuan").hide();
                }
                $("#kode_transaksi").val("");
                $("#telpon").val("");
                $("#btn-progress").html('Cek Cucian');
                $("#modal_progress").modal("show");
            } else {
                $("#kode_transaksi").val("");
                $("#telpon").val("");
                $("#title-failed").html(response.status);
                $("#message-failed").html(response.message);
                $("#btn-progress").html('Cek Cucian');
                $("#modal-danger").modal("show");
            }
         },
         error: function (response) {
          $("#btn-progress").html('Cek Cucian');
            alert(response);
         },
      });
      
      
    });

    function formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
  </script>
</html>
