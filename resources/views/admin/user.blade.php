<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.head')
  </head>
  <body class="antialiased">
    <div class="wrapper">
      @include('layout.header')

      <div class="page-wrapper">
        <div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  LaundryKu
                </div>
                <h2 class="page-title">
                  Data Petugas
                </h2>
              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="container-xl">
            <div class="row row-cards">
              <div class="col-md-6 col-xl-4">
                <div class="row row-cards">
                  <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                         <h3 class="card-title">Tambah Petugas Laundry</h3>
                      </div>
                      <div class="card-body">
                      @if(Session::has('flash_message'))
                        <div class="alert {{ Session::get('alert') }} alert-dismissible" role="alert">
                            <div class="d-flex">
                            <div>
                            </div>
                            <div>
                                <h4 class="alert-title" id="alert-title">{{ Session::get('flash_title') }}&hellip;</h4>
                                <div class="text-muted" id="alert-body">{{ Session::get('flash_message') }}</div>
                            </div>
                            </div>
                            <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
                        </div>
                      @endif
                      <form class="add-listing-form" action="{{ url('area/admin/tambah_petugas') }}" method="post" id="tambah-user">
                                @csrf
                                <div class="row">
                                <div class="col-md-6 col-xl-12">
                                    <div class="mb-3">
                                    <label class="form-label">Nama</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Ex. Ucup" autocomplete="off" required />
                                    </div>
                                    <div class="mb-3">
                                    <label class="form-label">Username</label>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Ex. Ucup123" autocomplete="off" required />
                                    </div>
                                    <div class="mb-3">
                                    <label class="form-label">No. Handphone</label>
                                    <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Ex. 082314617288" autocomplete="off" required />
                                    </div>
                                </div>
                                </div>
                                <div class="form-footer">
                                <div class="d-flex">
                                    <button type="submit" class="btn btn-primary ms-auto">Tambah</button>
                                </div>
                                </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-xl-8">
                <div class="row row-cards">
                  <div class="col-12">
                    <div class="card">
                      <div class="row row-0">
                        <div class="col">
                        <div class="card-header">
                         <h3 class="card-title">Data Petugas Laundry</h3>
                        </div>
                          <div class="card-body">
                          <div class="card-table table-responsive">
                          <table class="table table-vcenter card-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>No. Handphone</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($data_user->count() <= 0) {
                                  <tr>
                                    <td colspan="4">Tidak ada data petugas</td>
                                  </tr>
                                @endif
                                @foreach($data_user as $users)
                                <tr>
                                    <td>{{ ($data_user->currentPage()-1) * $data_user->perPage() + $loop->index + 1 }}</td>
                                    <td>{{ $users->name }}</td>
                                    <td>{{ $users->username }}</td>
                                    <td>{{ $users->phone_number }}</td>
                                    <td class="text-end">
                                        <div class="form-selectgroup">
                                            <a href="#" data-kode="{{ $users->id }}" id="deletePetugas">
                                            <label class="form-selectgroup-item">
                                                <span class="form-selectgroup-label">
	                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="4" y1="7" x2="20" y2="7" /><line x1="10" y1="11" x2="10" y2="17" /><line x1="14" y1="11" x2="14" y2="17" /><path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" /><path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /></svg>
                                                </span>
                                            </label>
                                            </a>
                                            <a href="#" data-id="{{ $users->username }}" id="updatePetugas">
                                            <label class="form-selectgroup-item">
                                                <span class="form-selectgroup-label">
	                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 15l8.385 -8.415a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3z" /><path d="M16 5l3 3" /><path d="M9 7.07a7.002 7.002 0 0 0 1 13.93a7.002 7.002 0 0 0 6.929 -5.999" /></svg>
                                                </span>
                                            </label>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('admin.modal') @include('layout.footer')
      </div>
       <!-- modal -->
    <div class="modal modal-blur fade" id="modal_edit_petugas" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="title">Edit Petugas Laundry</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                    @csrf
                    <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Nama</label>
                              <input type="text" class="form-control" name="nama_edt" id="nama_edt" placeholder="" autocomplete="off" required />
                              <input type="hidden" class="form-control" name="id" id="id_edt" required />
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Username</label>
                              <input type="text" class="form-control" name="username_edt" id="username_edt" placeholder="" autocomplete="off" required/>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">No. Handphone</label>
                              <input type="text" class="form-control" name="phone_edt" id="phone_edt" placeholder="" autocomplete="off" required />
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Password</label>
                              <input type="password" class="form-control" name="password" id="password" placeholder="" autocomplete="off"/>
                              <i><small>*Kosongkan jika password tidak ingin diubah</small></i>
                            </div>
                            
                          </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                    Cancel
                    </a>
                    <div class="ms-auto">
                        <button type="submit" class="btn btn btn-primary btn-sm" id="btn-edt">Update
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->
    </div>
   
    <script src="{{asset('dist/js/tabler.min.js')}}"></script>
    <script>
        $("body").on("click", "#deletePetugas", function (event) {
            if (confirm('Apakah anda yakin ingin hapus data ini?')) {
                event.preventDefault();
                var token = $('input[name="_token"]').val();
                var id = $(this).data("kode");
                let _url = "{{ url('area/admin/delete_petugas') }}";
                $.ajax({
                    url: _url,
                    type: "POST",
                    data: {
                        id: id,
                        _token: token
                    },
                    success: function (response) {
                    if (response.status == "00") {
                        $("#message-success").html(response.message);
                        $('#modal-success').modal('show');
                    } else {
                        $("#message-failed").html(response.message);
                        $('#modal-danger').modal('show');
                    }
                    },
                    error: function (response) {
                    alert(response);
                    },
                });
            }
        });
        
        $("body").on("click", "#updatePetugas", function (event) {
            event.preventDefault();
            var token = $('input[name="_token"]').val();
            var id = $(this).data("id");
            let _url = "{{ url('area/admin/detail_petugas') }}";
            $.ajax({
                url: _url,
                type: "POST",
                data: {
                    username: id,
                    _token: token
                },
                success: function (response) {
                    if (response.status == "00") {
                            $("#nama_edt").val(response.name);
                            $("#username_edt").val(response.username);
                            $("#phone_edt").val(response.phone);
                            $("#id_edt").val(response.id);
                            $('#username_edt').prop('readonly', true);
                            $('#modal_edit_petugas').modal('show');
                    } else {
                        $("#message-failed").html(response.message);
                        $('#modal-danger').modal('show');
                    }
                },
                error: function (response) {
                    alert(response);
                },
            });
        });
        
        $("body").on("click", "#btn-edt", function (event) {
            event.preventDefault();
            var id = $("#id_edt").val();
            var username = $("#username_edt").val();
            var phone = $("#phone_edt").val();
            var name = $("#nama_edt").val();
            var pass = $("#password").val();
            let _url = "{{ url('area/admin/update_petugas') }}";
            $.ajax({
                url: _url,
                type: "POST",
                data: {
                    id: id,
                    username: username,
                    name: name,
                    phone: phone,
                    password: pass,
                    _token: "{{ csrf_token() }}"
                },
                success: function (response) {
                    if (response.status == "00") {
                          $("#username_edt").val("");
                          $("#phone_edt").val("");
                          $("#nama_edt").val("");
                          $("#password").val("");
                          $("#id_edt").val("");
                          $('#modal_edit_petugas').modal('hide');
                          $("#message-success").html(response.message);
                          $('#modal-success').modal('show');
                    } else {
                          $("#username_edt").val("");
                          $("#phone_edt").val("");
                          $("#nama_edt").val("");
                          $("#password").val("");
                          $("#id_edt").val("");
                          $('#modal_edit_petugas').modal('hide');
                          $("#message-failed").html(response.message);
                          $('#modal-danger').modal('show');
                    }
                },
                error: function (response) {
                    alert(response);
                },
            });
        });
    </script>
  </body>
</html>
