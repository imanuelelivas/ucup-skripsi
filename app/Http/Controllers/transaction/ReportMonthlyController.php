<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use Excel;
use App\Models\TblTrxModel;
use App\Exports\MonthlyExport;
use App\Libraries\Mapping;
use Illuminate\Support\Facades\Log;

class ReportMonthlyController extends Controller
{
	function index(Request $request){
        if($request->get("month") != "" && $request->get("year") != "") {
          $date_start = $request->get("year").'-'.$request->get("month").'-01';
          $date_end = $request->get("year").'-'.$request->get("month").'-31';
          $dataTrx = TblTrxModel::whereDate('tgl_transaksi','>=',$date_start)->whereDate('tgl_transaksi','<=',$date_end);
          $pemasukan = $dataTrx->sum('harga');
          $fee = $dataTrx->sum('fee');
          $report = $dataTrx->orderBy('tgl_transaksi','ASC')->paginate(10);
        $data = array(
            'active_laporan'    => "active",
            'data_harian'       => $report,
            'pemasukan'         => $pemasukan,
            'fee'               => $fee,
            'month'               => $request->get("month"),
            'year'               => $request->get("year"),
            );
            return View('admin.report.report_monthly')->with($data);
          }else{
            $data = array(
              'active_laporan'    => "active",
              'data_harian'       => null,
              'pemasukan'         => '',
              'fee'               => '',
              'start'               => '',
              'end'               => '',
              );
              return View('admin.report.report_monthly')->with($data);
          }
          
	}

  public function exportExcel(Request $request) {
    $nama_file = 'laporan_bulanan_laundryku_'.$request->get("month").'_'.$request->get("year").'.xlsx';
    return Excel::download(new MonthlyExport, $nama_file);
  }

}
