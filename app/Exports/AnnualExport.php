<?php

namespace App\Exports;

use App\Models\TblTrxModel;
use App\Libraries\Mapping;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AnnualExport implements FromView
{
    public function view(): View
    {
        $date_start = request()->input('year').'-01-01';
        $date_end = request()->input('year').'-12-31';
        if(request()->input('year') != "") {
            $dataTrx = TblTrxModel::whereDate('tgl_transaksi','>=',$date_start)->whereDate('tgl_transaksi','<=',$date_end);
        }else{
            $dataTrx = TblTrxModel::whereDate('tgl_transaksi','>=',date('Y').'-01-01')->whereDate('tgl_transaksi','<=',date('Y').'-12-31');
        }
        $pemasukan = $dataTrx->sum('harga');
        $fee = $dataTrx->sum('fee');
        return view('admin.report.report_annual_excel', [
            'data_harian' => $dataTrx->get(),
            'pemasukan' => $pemasukan,
            'fee' => $fee,
            'year' => request()->input('year'),
        ]);
    }
}