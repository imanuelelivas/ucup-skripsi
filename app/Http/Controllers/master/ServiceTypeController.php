<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblJenisJasaModel;
use App\Libraries\Mapping;
use Illuminate\Support\Facades\Log;

class ServiceTypeController extends Controller
{
	function index(Request $request){
        $item_service = TblJenisJasaModel::orderBy('id','ASC')->paginate(10);
        $data_to_service = array(
            'data_service' => $item_service,
            'active_master'      =>  "active",
        );
        return View('admin.service_type')->with($data_to_service);
    }

    function act_add_items(Request $request) {
        $rules = array(
			'layanan'       => 'required',
			'harga_kiloan'  => 'required|numeric',
			'harga_satuan'  => 'required|numeric',
			'estimasi'      => 'required|numeric',
			'estimasi_desc' => 'required',
		);    
		$messages = array(
			'layanan.required'          => 'Jenis pakaian wajib diisi',
			'harga_kiloan.required'     => 'Harga wajib diisi',
			'harga_satuan.required'     => 'Harga satuan wajib diisi. bila tidak ada isi dengan 0',
			'estimasi.required'         => 'estimasi wajib diisi.',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
		    Session::flash('flash_message', $validator->errors()->first());
			Session::flash('flash_title', 'Ups gagal tambah Paket Layanan Laundry');
            Session::flash('alert', 'alert-danger');
		    return Redirect::back()->withInput();
		}

        if($request->input('estimasi') == '0'){
            Session::flash('flash_message', "Lama Pengerjaan tidak boleh 0");
			Session::flash('flash_title', 'Gagal');
            Session::flash('alert', 'alert-danger');
		    return Redirect::back()->withInput();
        }
        if($request->input('estimasi_desc') == "day"){
            $desc = "hari";
        }else{
            $desc = "jam";
        }
        $add_items = TblJenisJasaModel::create([
            'type_jasa'     => $request->input('layanan'),
            'estimasi'      => $request->input('estimasi'),
            'estimasi_desc' => $request->input('estimasi_desc'),
            'harga_kiloan'  => $request->input('harga_kiloan'),
            'harga_satuan'  => $request->input('harga_satuan'),
            'desc'  => 'Lama pengerjaan '. $request->input('estimasi').' '.$desc .' sejak cucian diterima',
        ]);

        if ($add_items) {
            Session::flash('flash_message', "Tambah ".$request->input('layanan')." berhasil");
            Session::flash('flash_title', 'berhasil');
            Session::flash('alert', 'alert-success');
            return Redirect('area/admin/service_type')->withInput();
        }else{
            Session::flash('flash_message', "Tambah ".$request->input('layanan')." gagal");
            Session::flash('flash_title', 'Gagal');
            Session::flash('alert', 'alert-danger');
            return Redirect('area/admin/service_type')->withInput();
        }
    }

    function delete(Request $request){
        $rules = array(
			'id' => 'required',
		);    
		$messages = array(
			'id.required'    => 'Kode pakaian wajib diisi',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
            return response()->json([
                'status' => "DNF",
                'message' => $validator->errors()->first(),
              ]);
		}
        $get_unit = TblJenisJasaModel::where('id',$request->input('id'))->first();
        if($get_unit == null){
            return response()->json([
                  'status' => "DNF",
                  'message' => 'Data yang akan dihapus tidak ditemukan'
                ]);
        }
        //delete transaksi
        $delete_unit = TblJenisJasaModel::where('id', $request->input('id'))->delete();
        if(!$delete_unit) {
          return response()->json([
              'status' => "DUF",
              'message' => '<b>'.$get_unit->type_jasa.'</b> Data gagal dihapus'
            ]);
        }
        return response()->json([
          'status' => "00",
          'message' => '<b>'.$get_unit->type_jasa.'</b> berhasil dihapus'
        ]);
	}

    function get(Request $request){
        $rules = array(
			'id' => 'required',
		);    
		$messages = array(
			'id.required'    => 'Kode layanan wajib diisi',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
            return response()->json([
                'status' => "DNF",
                'message' => $validator->errors()->first(),
              ]);
		}
        $get_jasa = TblJenisJasaModel::where('id',$request->input('id'))->first();
        if($get_jasa == null){
            return response()->json([
                  'status' => "DNF",
                  'message' => 'Data yang akan dihapus tidak ditemukan'
                ]);
        }
        return response()->json([
          'status' => "00",
          'message' => 'sukses',
          'layanan' => $get_jasa->type_jasa,
          'estimasi' => $get_jasa->estimasi,
          'estimasi_desc' => $get_jasa->estimasi_desc,
          'harga_kiloan' => $get_jasa->harga_kiloan,
          'harga_satuan' => $get_jasa->harga_satuan,
          'desc' => $get_jasa->desc,
          'id' => $get_jasa->id,
        ]);
	}

    function update(Request $request){
        $rules = array(
			'id'    => 'required|numeric',
			'layanan'  => 'required',
			'estimasi' => 'required|numeric',
			'estimasi_desc' => 'required',
			'harga_kiloan' => 'required|numeric',
			'harga_satuan' => 'required|numeric',
			'desc' => '',
		);    
		$messages = array(
			'id.required'          => 'Kode layanan wajib diisi',
			'layanan.required'     => 'layanan wajib diisi',
			'estimasi.required'    => 'estimasi wajib diisi',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
            return response()->json([
                'status' => "DNF",
                'message' => $validator->errors()->first(),
              ]);
		}

        if($request->input('estimasi_desc') == "day"){
            $desc = "hari";
        }else{
            $desc = "jam";
        }

        $update_jasa = TblJenisJasaModel::where('id', $request->input('id'))->update([
            'type_jasa'     => $request->input('layanan'),
            'estimasi'      => $request->input('estimasi'),
            'estimasi_desc' => $request->input('estimasi_desc'),
            'harga_kiloan'  => $request->input('harga_kiloan'),
            'harga_satuan'  => $request->input('harga_satuan'),
             'desc'         => 'Lama pengerjaan '. $request->input('estimasi').' '.$desc .' sejak cucian diterima',
        ]);
    
        if(!$update_jasa) {
            return response()->json([
                'status' => "UDF",
                'message' => 'Layanan <b>'.$request->input('layanan')."</b> gagal diupdate",
                ]);
        }
        return response()->json([
          'status' => "00",
          'message' => 'Data berhasil diupdate',
        ]);
	}
}
