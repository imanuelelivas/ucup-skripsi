<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TblJenisPakaianModel extends Model {
    protected $table = 'tbl_jenis_laundry_satuan';


    protected $fillable = ['jenis_pakaian','harga_satuan','created_date'];

    public $timestamps = false;

    public $incrementing = false;
    
} 