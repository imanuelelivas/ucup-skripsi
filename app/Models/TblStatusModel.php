<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TblStatusModel extends Model {
    protected $table = 'tbl_status';


    protected $fillable = ['group','status','status_desc','style_desc'];

    public $timestamps = false;

    public $incrementing = false;
    
} 