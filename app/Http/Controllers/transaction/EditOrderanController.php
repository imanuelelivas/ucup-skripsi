<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblTrxModel;
use App\Models\TblJenisJasaModel;
use App\Models\TblJenisCucianModel;
use App\Models\TblLaundrySatuanModel;
use Illuminate\Support\Facades\Log;

class EditOrderanController extends Controller
{
	function edit(Request $request) {
		$kode = $request->code;
		if($kode != "") {	
			$estimasi = TblJenisJasaModel::get();
			$jenis_cucian = TblJenisCucianModel::get();
			$trx = TblTrxModel::where('id_transaksi',$kode)->first();
			if($trx == null){
				Session::flash('flash_message', "Data tidak ditemukan");
				Session::flash('color_card', 'red');
				Session::flash('color_text', 'white-text');
				return Redirect::back()->withInput();
			}

			if($trx->jenis_cucian == '1') {
				$data = array(
					'estimasi'      =>  $estimasi,
					'jenis_cucian'  =>  $jenis_cucian,
					'data_trx'      =>  $trx,
					'active_trx'    =>  "active",
				);
				return View('admin.edit_order_kiloan')->with($data);
			}
			// return to view edit satuan
			$cucian_satuan = TblLaundrySatuanModel::where('id_transaksi',$kode)->get();
			$harga_total = TblLaundrySatuanModel::where('id_transaksi',$kode)->sum('total');
			$data = array(
				'estimasi'      =>  $estimasi,
				'jenis_cucian'  =>  $jenis_cucian,
				'data_trx'      =>  $trx,
				'cucian_satuan' =>  $cucian_satuan,
				'harga_total' 	=>  $harga_total,
				'active_trx'    =>  "active",
			);

			return View('admin.edit_order_satuan')->with($data);

		}else{
			Session::flash('flash_message', "Data tidak ditemukan");
			Session::flash('color_card', 'red');
			Session::flash('color_text', 'white-text');
			return Redirect::back()->withInput();
		}
	}

	function act_edit(Request $request){
		$rules = array(
			'kode'       	=> 'required',
			'nama'       	=> 'required',
			'telpon'    	=> 'required|numeric',
			'jenis_cucian' 	=> 'required|numeric',
			'berat'    		=> '',
			'estimasi'    	=> 'numeric',
			'alamat'		=> '',
		);    
		$messages = array(
			'nama.required'       	=> 'Nama wajib diisi',
			'telpon.required'    	=> 'telpon wajib diisi',
			'jenis_cucian.required'	=> 'jenis cucian wajib diisi',
		);
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
		  Session::flash('flash_message', $validator->errors()->first());
		  Session::flash('color_card', 'red');
		  Session::flash('color_text', 'white-text');
		  return Redirect::back()->withInput();
		}

		$trx = TblTrxModel::where('id_transaksi',$request->input('kode'))->first();
		if($trx == null){
			Session::flash('flash_message', "Estimasi Pengerjaan tidak valid");
			Session::flash('color_card', 'red');
			Session::flash('color_text', 'white-text');
			return Redirect::back()->withInput();
		}
		$estimasi = TblJenisJasaModel::where('id',$request->input('estimasi'))->first();
		if($estimasi == null){
			Session::flash('flash_message', "Estimasi Pengerjaan tidak valid");
			Session::flash('color_card', 'red');
			Session::flash('color_text', 'white-text');
			return Redirect::back()->withInput();
		}

		$berat = $request->input('berat');
		if($request->input('berat') < 2){
			$berat = 2;
		}
		// harga
		$harga = $berat * $estimasi->harga_kiloan;
		$tgl_bayar = NULL;
		if($request->input('pembayaran') == '1') {
			$tgl_bayar = date('Y-m-d H:i:s');
		}
		// action update data
		$update_orderan = TblTrxModel::where('id_transaksi', $request->input('kode'))->update([
			'nama_costumer'     => $request->input('nama'),
			'nomor_tlp'     	=> $request->input('telpon'),
			'alamat'	     	=> $request->input('alamat'),
			'type_jasa'        	=> $request->input('estimasi'),
			'berat_cucian'      => $request->input('berat'),
			'harga'        		=> ceil($harga),
			'status_pembayaran' => $request->input('pembayaran'),
			'tgl_pembayaran' 	=> $tgl_bayar,
			'update_by' 		=> Session::get('username'),
			'tgl_update' 		=> date('Y-m-d H:i:s'),
		  ]);
		
		// fail update data
		if (!$update_orderan) {
			Session::flash('flash_message'," Edit data cucian ".$request->input('kode')." gagal");
			Session::flash('flash_title', 'Gagal');
			return Redirect('area/admin/home')->withInput();
		}
		// success
		Session::flash('flash_message', $request->input('kode')." - edit data cucian ".$request->input('kode')." berhasil");
		Session::flash('flash_title', 'berhasil');
		return Redirect('area/admin/home')->withInput();
	}

	function act_edit_satuan(Request $request){
		$rules = array(
			'kode'       	=> 'required',
			'nama'       	=> 'required',
			'telpon'    	=> 'required|numeric',
			'jenis_cucian' 	=> 'required|numeric',
			'berat'    		=> '',
			'estimasi'    	=> 'numeric',
			'alamat'		=> '',
			'jsatuan'   => 'required|array',
			'jpcs'    	=> 'required|array',
			'jharga' 	=> 'required|array',
		);    
		$messages = array(
			'nama.required'       	=> 'Nama wajib diisi',
			'telpon.required'    	=> 'telpon wajib diisi',
			'jenis_cucian.required'	=> 'jenis cucian wajib diisi',
		);
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
		  Session::flash('flash_message', $validator->errors()->first());
		  Session::flash('color_card', 'red');
		  Session::flash('color_text', 'white-text');
		  return Redirect::back()->withInput();
		}
		$kode = $request->input('kode');
		$trx = TblTrxModel::where('id_transaksi',$request->input('kode'))->first();
		if($trx == null){
			Session::flash('flash_message', "Estimasi Pengerjaan tidak valid");
			Session::flash('color_card', 'red');
			Session::flash('color_text', 'white-text');
			return Redirect::back()->withInput();
		}
		$estimasi = TblJenisJasaModel::where('id',$request->input('estimasi'))->first();
		if($estimasi == null){
			Session::flash('flash_message', "Estimasi Pengerjaan tidak valid");
			Session::flash('color_card', 'red');
			Session::flash('color_text', 'white-text');
			return Redirect::back()->withInput();
		}

		$delete_satuan = TblLaundrySatuanModel::where('id_transaksi',$kode)->delete();
		if(!$delete_satuan){
			Session::flash('flash_message', "Update Data Satuan Gagal");
			Session::flash('color_card', 'red');
			Session::flash('color_text', 'white-text');
			return Redirect::back()->withInput();
		}

		$pakaian = $request->input('jsatuan');
		$jpcs = $request->input('jpcs');
		$jharga = $request->input('jharga');
		for($count = 0; $count < count($pakaian); $count++) {
			$data = array(
				'id_transaksi' 	=> $kode,
				'item' 			=> $pakaian[$count],
				'qty'  			=> $jpcs[$count],
				'total'  		=> $jpcs[$count]*$jharga[$count],
			);

			$insert_data[] = $data; 
		}
		$addLaudrySatuan = TblLaundrySatuanModel::insert($insert_data);
		if(!$addLaudrySatuan){
			Session::flash('flash_message', "Update Data Satuan Gagal - insert to table");
			Session::flash('color_card', 'red');
			Session::flash('color_text', 'white-text');
			return Redirect::back()->withInput();
		}

		$tgl_bayar = NULL;
		if($request->input('pembayaran') == '1') {
			$tgl_bayar = date('Y-m-d H:i:s');
		}
		$jsa = TblJenisJasaModel::where('id',$request->input('estimasi'))->first();
		$harga = TblLaundrySatuanModel::select(TblLaundrySatuanModel::raw("SUM(total) as total_harga"))
	    ->where("id_transaksi",$kode)->get();
		// action update data
		$update_orderan = TblTrxModel::where('id_transaksi', $request->input('kode'))->update([
			'nama_costumer'     => $request->input('nama'),
			'nomor_tlp'     	=> $request->input('telpon'),
			'alamat'	     	=> $request->input('alamat'),
			'type_jasa'        	=> $request->input('estimasi'),
			'harga'        		=> $harga[0]->total_harga+$jsa->harga_satuan,
			'status_pembayaran' => $request->input('pembayaran'),
			'tgl_pembayaran' 	=> $tgl_bayar,
			'update_by' 		=> Session::get('username'),
			'tgl_update' 		=> date('Y-m-d H:i:s'),
		  ]);
		
		// fail update data
		if (!$update_orderan) {
			Session::flash('flash_message'," Edit data cucian ".$request->input('kode')." gagal");
			Session::flash('flash_title', 'Gagal');
			return Redirect('area/admin/home')->withInput();
		}
		// success
		Session::flash('flash_message', $request->input('kode')." - edit data cucian ".$request->input('kode')." berhasil");
		Session::flash('flash_title', 'berhasil');
		return Redirect('area/admin/home')->withInput();
	}

}
