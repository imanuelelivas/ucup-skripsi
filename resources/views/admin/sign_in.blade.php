<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.head')
  </head>
  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="page page-center">
      <div class="container-tight py-4">
        <div class="text-center mb-4">
          <a href="."><img src="{{asset('dist/img/logoku.png')}}" height="36" alt=""></a>
        </div>
        <form class="card card-md" action="{{url('act/login')}}" method="post" autocomplete="off">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Login LaundryKu</h2>
            @if(Session::has('flash_message'))
            <div class="alert alert-danger alert-dismissible" role="alert">
                <div class="d-flex">
                  <div>
                  </div>
                  <div>
                    <h4 class="alert-title" id="alert-title">{{ Session::get('flash_title') }}&hellip;</h4>
                    <div class="text-muted" id="alert-body">{{ Session::get('flash_message') }}</div>
                  </div>
                </div>
                <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
            </div>
            @endif
            <div class="mb-3">
              <label class="form-label">Username</label>
              @csrf
              <input type="text" class="form-control" placeholder="Username" name="username_int" required>
            </div>
            <div class="mb-2">
              <label class="form-label">
                Password
                
              </label>
              <div class="input-group input-group-flat">
                <input type="password" class="form-control"  placeholder="Password" name="password_int"  autocomplete="off" required>
              </div>
            </div>
            <div class="form-footer">
              <button type="submit" class="btn btn-primary w-100">Log In</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="./dist/js/tabler.min.js"></script>
  </body>
</html>
