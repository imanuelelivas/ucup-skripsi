<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TblTrxModel extends Model
{
	protected $table = 'tbl_transaksi';


	protected $fillable = ['id_transaksi','petugas','nama_costumer','nomor_tlp','jenis_cucian','type_jasa','berat_cucian','tgl_transaksi','tgl_selesai','tgl_diambil','harga','status_cucian','status_pembayaran','tgl_update','catatan','tgl_diambil','tgl_pembayaran','fee','fee_status','alamat',
	];

	public $timestamps = false;

	public $incrementing = false;
} 