<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.head')
  </head>
  <body class="antialiased">
    <div class="wrapper">
      @include('layout.header')

      <div class="page-wrapper">
        <div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  LaundryKu
                </div>
                <h2 class="page-title">
                  Setting Akun
                </h2>
              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="container-xl">
            <div class="row row-cards">
              <div class="col-md-5 col-xl-5">
                <div class="row row-cards">
                  <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                         <h3 class="card-title">Setting Password</h3>
                      </div>
                      <div class="card-body">
                      @if(Session::has('flash_message'))
                        <div class="alert {{ Session::get('alert') }} alert-dismissible" role="alert">
                            <div class="d-flex">
                            <div>
                            </div>
                            <div>
                                <h4 class="alert-title" id="alert-title">{{ Session::get('flash_title') }}&hellip;</h4>
                                <div class="text-muted" id="alert-body">{{ Session::get('flash_message') }}</div>
                            </div>
                            </div>
                            <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
                        </div>
                      @endif
                      <form class="add-listing-form" action="{{ url('area/admin/ganti_password') }}" method="post" id="edit-password" onsubmit="return cek_pass_is_same();">
                      @csrf
                        <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Password Lama</label>
                              <input type="password" class="form-control" name="pass_old" id="pass_old" autocomplete="off" required />
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Password Baru</label>
                              <input type="password" class="form-control" name="pass_new" id="pass_new" autocomplete="off" value="" required />
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="mb-3">
                              <label class="form-label">Konfirmasi Password Baru</label>
                              <input type="password" class="form-control" name="pass_new_again" id="pass_new_again" autocomplete="off" value="" required />
                              <div id="message_pass" class="text-danger"></div>
                            </div>
                          </div>
                        </div>
                        <div class="form-footer">
                          <div class="d-flex">
                            <button type="submit" class="btn btn-primary ms-auto">Ganti Password</button>
                          </div>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('admin.modal') 
        @include('layout.footer')
      </div>
    </div>
    <script src="{{asset('dist/js/tabler.min.js')}}"></script>
    <script>
       function cek_pass_is_same() {
        if (document.getElementById('pass_new_again').value ==
                document.getElementById('pass_new').value) {
                return true;
        }else{
              $("#message_pass").html("<i>*Konfirmasi Password baru salah</i>")
                return false;
            }
        };
    </script>
  </body>
</html>
