<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TblLaundrySatuanModel extends Model
{
	protected $table = 'tbl_laudry_satuan';



	protected $fillable = ['id_transaksi','item','qty','total'
	];

	public $timestamps = false;

	public $incrementing = false;
} 