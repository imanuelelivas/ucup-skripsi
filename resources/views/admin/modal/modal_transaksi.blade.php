<div class="modal modal-blur fade" id="modal-ambil" tabindex="-1" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         <div class="modal-status bg-success"></div>
         <div class="modal-body text-center py-4">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-green icon-lg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
               <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
               <circle cx="12" cy="12" r="9"></circle>
               <path d="M9 12l2 2l4 -4"></path>
            </svg>
            <h3>Sukses</h3>
            <div class="text-muted" id="message-success">sukses</div>
         </div>
         <div class="modal-footer">
            <div class="w-100">
               <div class="row">
                  <div class="col">
                     <a href="#" class="btn btn-success w-100" data-bs-dismiss="modal" id="ok-success">
                        OK
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
   $('#ok-success').click(function() {
    location.reload();
   });
   
   $('#ok-failed').click(function() {
    location.reload();
   });
</script>
