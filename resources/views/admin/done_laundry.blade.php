<!doctype html>
<html lang="en">
  <head>
    @include('layout.head')

  </head>
  <body class="antialiased">
    <div class="wrapper">

      @include('layout.header')

      <div class="page-wrapper">
        <div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  LaundryKu
                </div>
                <h2 class="page-title">
                  Cucian telah diambil
                </h2>
              </div>
              <div class="col-auto ms-auto d-print-none">
                <div class="btn-list">
                  <a href="{{ url('area/admin/form_order') }}" class="btn btn-primary d-none d-sm-inline-block">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                    Tambah Cucian
                  </a>
                  <a href="{{ url('area/admin/form_order') }}" class="btn btn-primary d-sm-none btn-icon" aria-label="Tambah Cucian">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="container-xl">
            <div class="row row-deck row-cards">
              <!-- disini -->
              <div class="col-md-12 col-lg-12">
                <div class="card">
                <div class="card-status-top bg-red"></div>
                  <div class="card-header">
                    <h3 class="card-title">Data cucian telah diambil</h3>
                  </div>
                  <form>
                    <div class="card-body border-bottom py-3">
                      <div class="d-flex">
                      <div class="ms-auto text-muted">
                          <div class="ms-2 d-inline-block">
                            <select class="form-select" name="type">
                              <option value="1" {{ request()->get('type') === '1' ? 'selected' : '' }}>Kode Transaksi</option>
                              <option value="2" {{ request()->get('type') === '2' ? 'selected' : '' }}>Nama</option>
                              <option value="3" {{ request()->get('type') === '3' ? 'selected' : '' }}>No. Handphone</option>
                            </select>
                          </div>
                          <div class="ms-2 d-inline-block">
                            <input type="text" class="form-control" aria-label="Search invoice" name="query" placeholder="Search" value=" {{ request()->get('query') }}"> 
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  <div class="card-table table-responsive">
                    <table class="table card-table table-vcenter">
                      <thead>
                          <tr>
                            <th>Tgl. Pengambilan</th>
                            <th>Kode Transaksi</th>
                            <th>Nama Pelanggan</th>
                            <th>Harga</th>
                            <th>Denda</th>
                            <th>Pembayaran</th>
                            <th></th>
                          </tr>
                      </thead>
                      @if ($data_order->count() <= 0)
                        <tr>
                          <td colspan="7">Cucian Selesai tidak ada</td>
                        </tr>
                      @endif
                      @foreach ($data_order as $order)
                          <tr>
                          <td>
                            <?php
                                $date_end = date_create($order->tgl_diambil);
                                echo date_format($date_end,"d/m/Y H:i:s");
                              ?>
                            </td>
                            <td><a href="#" data-id="{{ $order->id}}" id="detailOId" >{{$order->id_transaksi}}</a>
                            <div class="text-muted">{{Mapping::mapping_estimasi($order->type_jasa)}}</div>
                            </td>
                            <td>{{$order->nama_costumer}}</td>
                            <td>
                              <div>Rp {{number_format($order->harga)}}
                                <?php
                                  echo "<input type='hidden' id='hrg_".$order->id."' value='".$order->harga."'>";
                                ?>
                              </div>
                            </td>
                            <td>Rp {{number_format($order->fee)}}</td>
                            <td class="w-20">
                            @if($order->status_pembayaran == '1') 
                              Lunas
                            @else
                              Belum dibayar
                            @endif
                            </td>
                          </tr>
                        @endforeach
                    </table>
                    <br>
						          {{ $data_order->links() }}
                  </div>
                </div>
              </div>
          </div>
        </div>
         <!-- modal -->
         @include('admin.modal_detail_done')
				<!-- end modal -->
        @include('admin.modal')
        @include('admin.modal.modal_transaksi')

        @include('layout.footer')
        <!-- modal -->
        <div class="modal modal-blur fade" id="modal_fee" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="title">Denda Telat Ambil Cucian</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="byr_fee">
                        @csrf
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Telat Ambil</label>
                                  <input type="text" class="form-control" name="deadline" id="deadline" autocomplete="off" value="" required />
                                </div>
                              </div>
                            </div>
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Total Denda</label>
                                  <input type="text" class="form-control" name="fee" id="fee" autocomplete="off" value="" required />
                                  <input type="hidden" class="form-control" name="id_edt_fee" id="id_edt_fee" required />
                                </div>
                              </div>
                            </div>
                            <i class="text-red">* telat ambil cucian denda Rp 2.000/hari</i>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                            </a>
                            <div class="ms-auto">
                                <button type="button" class="btn btn btn-primary btn-sm" id="btn-bayar">Bayar
                                </button>
                            </div>
                        </div>
                        </form>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <!-- modal bayar -->
        <div class="modal modal-blur fade" id="modal_bayar" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="title">Form Pembayaran</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="byr_do">
                        @csrf
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Harga Cucian</label>
                                    <input type="text" class="form-control" id="hrg_cucian" autocomplete="off" required>
                                </div>
                              </div>
                        </div>
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Telat Ambil</label>
                                    <input type="text" class="form-control" id="tlt_byr" autocomplete="off" required>
                                </div>
                              </div>
                            </div>
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Denda Telat Ambil</label>
                                    <input type="text" class="form-control" autocomplete="off" id="tlt_fee_byr">
                                  <input type="hidden" class="form-control" name="id_edt_fee_byr" id="id_edt_fee_byr" required/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Total Pembayaran</label>
                                  <div class="input-group mb-2">
                                    <span class="input-group-text">
                                      Rp
                                    </span>
                                    <input type="text" class="form-control" id="ttl_byr" autocomplete="off" required>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Status Pembayaran</label>
                                    <select class="form-select" id="sts_byr">
                                      <option value="">-Pilih-</option>
                                      <option value="1">Lunas</option>
                                    </select>
                                </div>
                              </div>
                            </div>
                            <i class="text-red">* telat ambil cucian denda Rp 2.000/hari</i>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                            </a>
                            <div class="ms-auto">
                                <button type="button" class="btn btn btn-primary btn-sm" id="btn-bayar-do">Bayar
                                </button>
                            </div>
                        </div>
                        </form>
                </div>
            </div>
        </div>
        <!-- end modal -->
      </div>
    </div>
    <script src="{{asset('dist/js/tabler.min.js')}}"></script>
  </body>
</html>