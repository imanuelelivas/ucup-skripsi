<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblTrxModel;
use App\Models\TblJenisJasaModel;
use App\Models\TblJenisCucianModel;
use Illuminate\Support\Facades\Log;

class UpdateOrderanController extends Controller
{
	function update_pembayaran(Request $request){
        $update_orderan = false;
        $get_orderan = TblTrxModel::where('id',$request->input('id'))->first();
        if($get_orderan == null){
            return response()->json([
                  'status' => "01",
                  'title' => "Gagal",
                  'message' => 'data tidak ditemukan'
                ]);
        }

        if($get_orderan->status_pembayaran == '1'){
          return response()->json([
            'status' => "01",
            'title' => "Laundry Sudah Lunas!!!",
            'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> status pembayaran tidak bisa diupdate karena telah Lunas",
          ]);
        }

        if($request->input('status_pembayaran') == ""){
            return response()->json([
              'status' => "01",
              'title' => "Gagal",
              'message' => 'Status Pembayaran gagal diupdate. Status Pembayaran wajib ada',
            ]);
        }

        if($get_orderan->status_cucian == '5' && $request->input('status_pembayaran') == '0'){
          return response()->json([
            'status' => "01",
            'title' => "Gagal!!!",
            'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> status pembayaran tidak bisa diupdate karena sudah <b>LUNAS</b> dan telah diambil",
          ]);
        }

        $update_orderan = TblTrxModel::where('id', $request->input('id'))->update([
            'status_pembayaran'  => $request->input('status_pembayaran'),
            'catatan'            => $request->input('catatan'),
            'tgl_update'         => date('Y-m-d H:i:s'),
            'update_by'          => Session::get('username'),
          ]);
        
        if(!$update_orderan) {
          return response()->json([
              'status' => "01",
              'title' => "Gagal",
              'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> status pembayaran GAGAL diupdate",
            ]);
        }

        return response()->json([
          'status' => "00",
          'title' => "Sukses",
          'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> status pembayaran BERHASIL diupdate",
        ]);
	}

  function update_status_cucian(Request $request) {
    $update_orderan = false;
    $get_orderan = TblTrxModel::where('id',$request->input('id'))->first();
    if($get_orderan == null){
        return response()->json([
              'status' => "01",
              'title' => "Gagal",
              'message' => 'data tidak ditemukan'
            ]);
    }

    if($request->input('status_cucian') == ""){
        return response()->json([
          'status' => "01",
          'title' => "Gagal",
          'message' => 'Status Cucian gagal diupdate. Status cucian wajib ada',
        ]);
    }

    if($get_orderan->status_pembayaran == '0' && $request->input('status_cucian') == '5'){
      return response()->json([
        'status' => "01",
        'title' => "Laundry Belum Dibayar!!!",
        'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> status cucian tidak bisa diupdate (Diambil) karena belum Lunas",
      ]);
    }
    $update_orderan = TblTrxModel::where('id', $request->input('id'))->update([
        'status_cucian'  => $request->input('status_cucian'),
        'catatan'            => $request->input('catatan'),
        'tgl_update'         => date('Y-m-d H:i:s'),
        'update_by'         => Session::get('username'),
      ]);
    
    if(!$update_orderan) {
      return response()->json([
          'status' => "01",
          'title' => "Gagal",
          'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> status cucian GAGAL diupdate",
        ]);
    }

    return response()->json([
      'status' => "00",
      'title' => "Sukses",
      'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> status cucian BERHASIL diupdate",
    ]);
  }

}
