<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TblJenisCucianModel extends Model {
	protected $table = 'tbl_jenis_cucian';

	protected $fillable = ['type'];

	public $timestamps = false;

	public $incrementing = false;

	public function get_type_laudry(){
		$query = DB::table($this->table)->select('*');
		$result = $query->get();
		return $result;        
	}

} 