<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblTrxModel;
use App\Models\TblJenisJasaModel;
use App\Models\TblJenisCucianModel;
use App\Models\TblStatusModel;
use App\Models\TblLaundrySatuanModel;
use App\Libraries\Mapping;
use Illuminate\Support\Facades\Log;

class StrukController extends Controller
{
	function struk(Request $request){
        $kode = $request->code;
        $name = $request->costumer;
        $data_trx = TblTrxModel::where('id_transaksi',$kode)->first();
        if($data_trx == null){
            Session::flash('flash_message', "Data tidak ditemukan");
		    Session::flash('color_card', 'red');
		    Session::flash('color_text', 'white-text'); 
		    return Redirect('/')->withInput();
        }
        $item_satuan = TblLaundrySatuanModel::where('id_transaksi',$kode)->get();
        $data_to_struk = array(
            'kode' => $data_trx->id_transaksi,
            'nama' => $data_trx->nama_costumer,
            'no_tlp' => $data_trx->nomor_tlp,
            'alamat' => $data_trx->alamat,
            'jenis_cucian' => $data_trx->jenis_cucian,
            'jenis_cucian_desc' => Mapping::mapping_jenis_cucian($data_trx->jenis_cucian),
            'estimasi' => $data_trx->type_jasa,
            'estimasi_desc' => Mapping::mapping_estimasi($data_trx->type_jasa),
            'berat' => $data_trx->berat_cucian,
            'tgl_transaksi' => $data_trx->tgl_transaksi,
            'tgl_selesai' => $data_trx->tgl_selesai,
            'tgl_diambil' => $data_trx->tgl_diambil,
            'harga' => $data_trx->harga,
            'status_cucian' => Mapping::mapping_status_cucian($data_trx->status_cucian)[0],
            'status_pembayaran' => Mapping::mapping_pembayaran($data_trx->status_pembayaran)[0],
            'tgl_pembayaran' => $data_trx->tgl_pembayaran,
            'tgl_update' => $data_trx->tgl_update,
            'catatan' => $data_trx->catatan,
            'petugas' => $data_trx->petugas,
            'fee' => $data_trx->fee,
            'fee_status' => $data_trx->fee_status,
            'items' => $item_satuan,
            'tgl_print' => date('d/m/Y H:i:s'),
        );
        return View('admin.struk')->with($data_to_struk);
    }
}
