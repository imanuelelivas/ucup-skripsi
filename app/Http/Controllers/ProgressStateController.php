<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Models\TblTrxModel;
use App\Models\TblJenisJasaModel;
use App\Models\TblJenisCucianModel;
use App\Models\TblStatusModel;
use App\Models\TblLaundrySatuanModel;
use App\Models\TblAdminModel;
use App\Libraries\Mapping;
use App\Libraries\Generate;
use Illuminate\Support\Facades\Log;

class ProgressStateController extends Controller
{
    function progress(Request $request){
        $rules = array(
			'kode_transaksi' => 'required',
		);    
		$messages = array(
			'kode_transaksi.required' => 'ID transaksi diperlukan untuk cek cucian',
		);
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
			return response()->json([
				'status' => "DNF",
				'message' => $validator->errors()->first(),
			  ]);
		}
		$kode = $request->input('kode_transaksi');
        $get_transaksi = TblTrxModel::where('id_transaksi',$kode)->first();
		if($get_transaksi == null){
			return response()->json([
				'status' => "DNF",
				'message' => 'Laundry dengan kode transaksi <b>'.$kode.'</b> tidak ditemukan',
			  ]);
		}
		$get_laundy_satuan = TblLaundrySatuanModel::where('id_transaksi',$kode)->get();
		$dts_laudry =[];
		foreach ($get_laundy_satuan as $key => $value) {
		$dts_laudry[$key]['id'] = $value->id;
		$dts_laudry[$key]['id_transaksi'] = $value->id_transaksi;
		$dts_laudry[$key]['item'] = $value->item;
		$dts_laudry[$key]['qty'] = $value->qty;
		$dts_laudry[$key]['total'] = $value->total;
		}
		return response()->json([
			'status' => "00",
			'message' => 'Laundry dengan kode transaksi '.$kode.' ditemukan',
			'data_laundry' => [
				'kode'			=> $get_transaksi->id_transaksi,
				'nama'			=> $get_transaksi->nama_costumer, 
				'telpon'		=> $get_transaksi->nomor_tlp,
				'alamat'		=> $get_transaksi->alamat,
				'jenis_cucian'	=> Mapping::mapping_jenis_cucian($get_transaksi->jenis_cucian),
				'type_layanan'	=> Mapping::estimasi_harga($get_transaksi->type_jasa)[0],
				'berat_cucian'	=> $get_transaksi->berat_cucian,
				'tgl_transaksi'	=> $get_transaksi->tgl_transaksi,
				'tgl_selesai'	=> $get_transaksi->tgl_selesai,
				'harga'			=> $get_transaksi->harga,
				'status_cucian'	=> Mapping::mapping_status_cucian($get_transaksi->status_cucian)[0],
				'status_pembayaran'	=> Mapping::mapping_pembayaran($get_transaksi->status_pembayaran)[0],
				'tgl_pembayaran'	=> $get_transaksi->tgl_pembayaran,
				'dts_laudry' => $dts_laudry,
			],
		  ]);  
    }
}
