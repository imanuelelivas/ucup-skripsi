<footer class="footer footer-transparent d-print-none">
          <div class="container">
            <div class="row text-center align-items-center flex-row-reverse">
              <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                <ul class="list-inline list-inline-dots mb-0">
                  <li class="list-inline-item">
                    Copyright &copy; 2021
                    <a href="." class="link-secondary">LaundyKu</a>.
                    All rights reserved.
                  </li>
                  <li class="list-inline-item">
                    <a href="#" class="link-secondary" rel="noopener">v0.0.1 Dev</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>