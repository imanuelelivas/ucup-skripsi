<!doctype html>
<html lang="en">
  <head>
    @include('layout.head')

  </head>
  <body class="antialiased">
    <div class="wrapper">

      @include('layout.header')

      <div class="page-wrapper">
        <div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  LaundryKu
                </div>
                <h2 class="page-title">
                  Laporan Bulanan
                </h2>
              </div>
              <div class="col-auto ms-auto d-print-none">
              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="container-xl">
            <div class="row row-deck row-cards">
              <!-- disini -->
              <div class="col-md-12 col-lg-12">
                <div class="card">
                <div class="card-status-top bg-red"></div>
                  <div class="card-header">
                    <h3 class="card-title">Laporan Bulanan</h3>
                  </div>
                  <div class="card-body">
                    <form>
                      <div class="card-body border-bottom py-3">
                        <div class="ms-auto text-muted">
                            <div class="ms-2 d-inline-block">
                            <label class="form-label">Pilih Bulan</label>
                            <div class="input-icon mb-2">
                               <select class="form-select" name="month" id="month" required>
                                <option value="">Pilih Bulan</option>
                                <option value="01" {{ (Request::get('month') == '01') ? 'selected': "" }}>Januari</option>
                                <option value="02" {{ (Request::get('month') == '02') ? 'selected': "" }}>Februari</option>
                                <option value="03" {{ (Request::get('month') == '03') ? 'selected': "" }}>Maret</option>
                                <option value="04" {{ (Request::get('month') == '04') ? 'selected': "" }}>April</option>
                                <option value="05" {{ (Request::get('month') == '05') ? 'selected': "" }}>Mei</option>
                                <option value="06" {{ (Request::get('month') == '06') ? 'selected': "" }}>Juni</option>
                                <option value="07" {{ (Request::get('month') == '07') ? 'selected': "" }}>Juli</option>
                                <option value="08" {{ (Request::get('month') == '08') ? 'selected': "" }}>Agustus</option>
                                <option value="09" {{ (Request::get('month') == '09') ? 'selected': "" }}>September</option>
                                <option value="10" {{ (Request::get('month') == '10') ? 'selected': "" }}>Oktober</option>
                                <option value="11" {{ (Request::get('month') == '11') ? 'selected': "" }}>November</option>
                                <option value="12" {{ (Request::get('month') == '12') ? 'selected': "" }}>Desember</option>
                               </select>
                              </div>
                            </div>
                            <div class="ms-2 d-inline-block">
                              <label class="form-label">Pilih Tahun</label>
                              <div class="input-icon mb-2">
                              <select class="form-select" name="year" id="year">
                                <option value="2021">2021</option>
                               </select>
                                </div>
                              </div>
                          </div>
                          <div class="ms-2 d-inline-block">
                              <label class="form-label">&nbsp;</label>
                              <div class="input-icon mb-2">
                                  <input type="submit" class="btn btn-md btn-primary" value="Lihat" />
                                </div>
                              </div>
                          </div>
                      </div>
                    </form>
                    @if ($data_harian != null)
                    <div class="card-table table-responsive">
                      <table class="table card-table table-vcenter">
                        <thead>
                            <tr>
                              <th>Tgl. Transaksi</th>
                              <th>Kode Transaksi</th>
                              <th>Nama Pelanggan</th>
                              <th>Pembayaran</th>
                              <th>Denda</th>
                              <th>Harga</th>
                              <th></th>
                            </tr>
                        </thead>
                        @if ($data_harian->count() <= 0)
                          <tr>
                            <td colspan="7">data tidak ditemukan</td>
                          </tr>
                        @endif
                        @foreach ($data_harian as $order)
                            <tr>
                            <td>
                              <?php
                                  $date_trx = date_create($order->tgl_transaksi);
                                  echo date_format($date_trx,"d/m/Y H:i:s");
                                ?>
                              </td>
                              <td><a href="#" data-id="{{ $order->id}}" id="detailOId" >{{$order->id_transaksi}}</a>
                              </td>
                              <td>{{$order->nama_costumer}}</td>
                              <td class="w-20">
                              @if($order->status_pembayaran == '1') 
                                Lunas
                              @else
                                Belum dibayar
                              @endif
                              </td>
                              <td>Rp {{number_format($order->fee)}}</td>
                              <td>
                                <div>Rp {{number_format($order->harga)}}
                                  <?php
                                    echo "<input type='hidden' id='hrg_".$order->id."' value='".$order->harga."'>";
                                  ?>
                                </div>
                              </td>
                            </tr>
                          @endforeach
                          <tr>
                            <td colspan="5" class="strong text-end">Pemasukan</td>
                            <td class="text-end">{{number_format($pemasukan)}}</td>
                          </tr>
                          <tr>
                            <td colspan="5" class="strong text-end">Denda</td>
                            <td class="text-end">{{number_format($fee)}}</td>
                          </tr>
                          <tr>
                            <td colspan="5" class="strong text-end">Total Pemasukan</td>
                            <td class="text-end">Rp {{number_format($pemasukan+$fee)}}</td>
                          </tr>
                          @if ($data_harian->count() > 0)
                            <tr>
                              <td colspan="6" class="strong text-end"><a href="{{ url('/area/admin/report/annual_to_excel'.'?year='.$year) }}" class="btn btn-sm btn-success">Download Laporan</a></td>
                            </tr>
                          @endif
                         
                      </table>
                      <br>
                        {{ $data_harian->appends(Request::all())->links() }}
                    </div>
                    @endif
                  </div>
                </div>
              </div>
          </div>
        </div>
        @include('admin.modal_detail_done')
        @include('layout.footer')
        <!-- end modal -->
      </div>
    </div>
    <script src="{{asset('dist/js/tabler.min.js')}}"></script>
  </body>
</html>