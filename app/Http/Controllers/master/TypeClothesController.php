<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblJenisPakaianModel;
use App\Libraries\Mapping;
use Illuminate\Support\Facades\Log;

class TypeClothesController extends Controller
{
	function index(Request $request){
        $item_satuan = TblJenisPakaianModel::orderBy('created_date','DESC')->paginate(10);
        $data_to_clothes = array(
            'data_item_satuan' => $item_satuan,
            'active_master'      =>  "active",
        );
        return View('admin.clothes_type')->with($data_to_clothes);
    }

    function act_add_items(Request $request) {
        $rules = array(
			'jenis' => 'required',
			'harga' => 'required|numeric',
		);    
		$messages = array(
			'jenis.required'    => 'Jenis pakaian wajib diisi',
			'harga.required'    => 'Harga wajib diisi',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
		    Session::flash('flash_message', $validator->errors()->first());
			Session::flash('flash_title', 'Ups gagal tambah laundry satuan');
            Session::flash('alert', 'alert-danger');
		    return Redirect::back()->withInput();
		}

        $add_items = TblJenisPakaianModel::create([
            'jenis_pakaian' => $request->input('jenis'),
            'harga_satuan'  => $request->input('harga'),
            'created_date'  => date('Y-m-d H:i:s'),
        ]);

        if ($add_items) {
            Session::flash('flash_message', "Tambah ".$request->input('jenis')." berhasil");
            Session::flash('flash_title', 'berhasil');
            Session::flash('alert', 'alert-success');
            return Redirect('area/admin/type_unit')->withInput();
        }else{
            Session::flash('flash_message', "Tambah ".$request->input('jenis')." gagal");
            Session::flash('flash_title', 'Gagal');
            Session::flash('alert', 'alert-danger');
            return Redirect('area/admin/type_unit')->withInput();
        }
    }

    function delete(Request $request){
        $rules = array(
			'id' => 'required',
		);    
		$messages = array(
			'id.required'    => 'Kode pakaian wajib diisi',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
            return response()->json([
                'status' => "DNF",
                'message' => $validator->errors()->first(),
              ]);
		}
        $get_unit = TblJenisPakaianModel::where('id',$request->input('id'))->first();
        if($get_unit == null){
            return response()->json([
                  'status' => "DNF",
                  'message' => 'Data yang akan dihapus tidak ditemukan'
                ]);
        }
        //delete transaksi
        $delete_unit = TblJenisPakaianModel::where('id', $request->input('id'))->delete();
        if(!$delete_unit) {
          return response()->json([
              'status' => "DUF",
              'message' => '<b>'.$get_unit->jenis_pakaian.'</b> Data gagal dihapus'
            ]);
        }
        return response()->json([
          'status' => "00",
          'message' => '<b>'.$get_unit->jenis_pakaian.'</b> berhasil dihapus'
        ]);
	}

    function get(Request $request){
        $rules = array(
			'id' => 'required',
		);    
		$messages = array(
			'id.required'    => 'Kode pakaian wajib diisi',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
            return response()->json([
                'status' => "DNF",
                'message' => $validator->errors()->first(),
              ]);
		}
        $get_unit = TblJenisPakaianModel::where('id',$request->input('id'))->first();
        if($get_unit == null){
            return response()->json([
                  'status' => "DNF",
                  'message' => 'Data yang akan dihapus tidak ditemukan'
                ]);
        }
        return response()->json([
          'status' => "00",
          'message' => 'sukses',
          'jenis' => $get_unit->jenis_pakaian,
          'harga' => $get_unit->harga_satuan,
          'id' => $get_unit->id,
        ]);
	}

    function update(Request $request){
        $rules = array(
			'id'    => 'required|numeric',
			'name'  => 'required',
			'price' => 'required|numeric',
		);    
		$messages = array(
			'id.required'       => 'Kode pakaian wajib diisi',
			'name.required'     => 'Nama pakaian wajib diisi',
			'price.required'    => 'Harga wajib diisi',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
            return response()->json([
                'status' => "DNF",
                'message' => $validator->errors()->first(),
              ]);
		}

        $update_unit = TblJenisPakaianModel::where('id', $request->input('id'))->update([
            'jenis_pakaian'  => $request->input('name'),
            'harga_satuan'   => $request->input('price'),
        ]);
    
        if(!$update_unit) {
            return response()->json([
                'status' => "UDF",
                'message' => '<b>'.$request->input('name')."</b> GAGAL diupdate",
                ]);
        }
        return response()->json([
          'status' => "00",
          'message' => 'Data berhasil diupdate',
        ]);
	}
}
