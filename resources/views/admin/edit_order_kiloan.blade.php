<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.head')
    <style>
        .select2-container {
          width: 100% !important;
        }
    </style>
<script type="text/javascript">
$(document).ready(function() {
      $('.cari').select2({
        minimumInputLength: 1,
        placeholder: 'Cari pakaian...',
        ajax: {
          url: "{{ url('area/admin/autocomplate') }}",
          dataType: 'json',
          delay: 100,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                return {
                  text: item.jenis_pakaian+"  ("+item.harga_satuan+"/pcs)",
                  id: item.harga_satuan
                }
              })
            };
          },
        }
      });
      });
</script>
  </head>
  <body class="antialiased">
    <div class="wrapper">
      @include('layout.header')
      <div class="page-wrapper">
        <div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  LaundryKu
                </div>
                <h2 class="page-title">
                  Edit Orderan Masuk
                </h2>
              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="container-xl">
            <div class="row row-deck row-cards">
            <div class="col-md-2"></div>
              <div class="col-md-8">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Edit Cucian</h3>
                  </div>
                  <div class="card-body">
                  @if(Session::has('flash_message'))
                  <div class="alert alert-danger alert-dismissible" role="alert">
                <div class="d-flex">
                  <div>
                  </div>
                  <div>
                    <h4 class="alert-title" id="alert-title">{{ Session::get('flash_title') }}&hellip;</h4>
                    <div class="text-muted" id="alert-body">{{ Session::get('flash_message') }}</div>
                  </div>
                </div>
                <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
            </div>
            @endif
                  <form class="add-listing-form" action="{{ url('area/admin/edit') }}" method="post" id="edit">
                    @csrf
                      <div class="row">
                          <div class="col-lg-8">
                            <div class="mb-3">
                              <label class="form-label">Nama</label>
                              <input type="text" class="form-control" name="nama" id="nama" placeholder="Ex. Alkalin" autocomplete="off" value="{{ $data_trx->nama_costumer}}"  required />
                              <input type="hidden" class="form-control" name="kode" id="kode" autocomplete="off" value="{{ $data_trx->id_transaksi}}" required />
                            </div>
                          </div>
                          <div class="col-lg-4">
                            <div class="mb-3">
                              <label class="form-label">No. Telpon</label>
                              <input type="text" class="form-control" name="telpon" placeholder="ex. 08231452765" maxlength="13" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="{{$data_trx->nomor_tlp}}" required />
                            </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-lg-12">
                            <div class="mb-3">
                              <label class="form-label">Alamat</label>
                              <textarea class="form-control" name="alamat" rows="2" placeholder="ex. Apartemen Green Bamboo no.12">{{$data_trx->alamat}}</textarea>
                            </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-lg-8">
                            <div class="mb-3">
                              <label class="form-label">Jenis Laundry</label>
                              <select class="form-select" name="jenis_cucian" id="jenis_cucian" required>
                                  <option value="1" selected>Kiloan</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-4" id="id_berat">
                            <div class="mb-3">
                              <label class="form-label">Berat(Kg)</label>
                              <input type="text" class="form-control" name="berat" id="berat" placeholder="Berat Cucian (Kg)" autocomplete="off" oninput="this.value=this.value.replace(/[^0-9.]/g,'');" value="{{$data_trx->berat_cucian}}" />
                            </div>
                          </div>
                      </div>
                      <label class="form-label">Estimasi Pengerjaan</label>
                      <div class="form-selectgroup form-selectgroup-boxes row mb-3">
                          @foreach ($estimasi as $js) 
                              <div class="col-lg-4">
                                <label class="form-selectgroup-item">
                                  <input type="hidden" class="form-control" id="harga_{{$js->id}}" value="{{$js->harga_kiloan}}"/>
                                  <input type="hidden" class="form-control" id="harga_s{{$js->id}}" value="{{$js->harga_satuan}}"/>
                                  <input type="radio" name="estimasi" value="{{$js->id}}" class="form-selectgroup-input" {{ $js->id === $data_trx->type_jasa ? "checked" : "" }} required/>
                                  <span class="form-selectgroup-label d-flex align-items-center p-3">
                                    <span class="me-3">
                                      <span class="form-selectgroup-check"></span>
                                    </span>
                                    <span class="form-selectgroup-label-content">
                                      <span class="form-selectgroup-title strong mb-1">{{$js->type_jasa}}</span>
                                      <span class="d-block text-muted">{{$js->desc}}</span>
                                    </span>
                                  </span>
                                </label>
                              </div>
                          @endforeach
                      </div>
                      <div class="row">
                          <div class="col-lg-8">
                            <div class="mb-3">
                              <label class="form-label">Pembayaran</label>
                              <select class="form-select" name="pembayaran" id="pembayaran" required>
                              <option value="">Pilih</option>
                              <option value="0"  {{ $data_trx->status_pembayaran == "0" ? "selected" : "" }}>Bayar Nanti</option>
                              <option value="1" {{ $data_trx->status_pembayaran == "1" ? "selected" : "" }}>Bayar Sekarang (Lunas)</option>
                              </select>
                            </div>
                          </div>
                      </div>
                      <div class="d-flex">
                      <div class="display-6 my-3 ms-auto" id="harga_js">Rp 0</div>
                      </div>
                      <div class="form-footer">
                        <div class="d-flex">
                          <button type="submit" class="btn btn-primary ms-auto">Edit Orderan</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer footer-transparent d-print-none">
          <div class="container">
            <div class="row text-center align-items-center flex-row-reverse">
              <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                <ul class="list-inline list-inline-dots mb-0">
                  <li class="list-inline-item">
                    Copyright &copy; 2021
                    <a href="." class="link-secondary">LaundyKu</a>. All rights reserved.
                  </li>
                  <li class="list-inline-item">
                    <a href="./changelog.html" class="link-secondary" rel="noopener">v0.0.1 Dev</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <script src="{{asset('dist/js/tabler.min.js')}}"></script>
    <script type="text/javascript">
      $(document).ready(function(){
      $("#harga_js").html("Rp "+formatNumber(<?php echo $data_trx->harga; ?>));
      if($("#jenis_cucian").val() == "1"){
          $('#id_berat').show();
          $('#form-satuan').hide();
          $('input:radio[name="estimasi"]').change(
            function(){
                if (this.checked) {
                  if($("#jenis_cucian :selected").val() == "1"){
                    if($("#berat").val() == ""){
                      $("#harga_js").html("Rp "+formatNumber(0));
                      alert("berat harus diisi");
                      $("#berat").focus();
                      return false;
                    }
                    else if(parseFloat($("#berat").val()) < 2){
                      $("#berat").val("");
                      $("#harga_js").html("Rp "+formatNumber(0));
                      alert("minimal berat cucian 2 Kg");
                      return false;
                    }
                    id_harga = "#harga_"+$("input[name='estimasi']:checked").val();
                    valuee = parseFloat($("#berat").val()) * parseInt($(id_harga).val());
                    harga = Math.ceil(valuee);
                    console.log(harga);
                    $("#harga_js").html("Rp "+formatNumber(harga));
                  }
                }
          });
          $('input[name=berat]').change(
              function() {
                if (!$("input[name='estimasi']:checked").val()) {
                    // $("#berat").val("");
                    $("#harga_js").html("Rp "+formatNumber(0));
                  // alert('silakan pilih estimasi pengerjaan terlebih dahulu');
                  //   return false;
                }
                else if(parseFloat($("#berat").val()) < 2){
                      $("#berat").val("");
                      $("#harga_js").html("Rp "+formatNumber(0));
                      alert("minimal berat cucian 2 Kg");
                      return false;
                    }
                else {
                    id_harga = "#harga_"+$("input[name='estimasi']:checked").val();
                    valuee = parseFloat($("#berat").val()) * parseInt($(id_harga).val());
                    harga = Math.ceil(valuee);
                    console.log(harga);
                    $("#harga_js").html("Rp "+formatNumber(harga));
                }
            });
        }
    });
    function formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
    </script>
  </body>
</html>
