<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TblJenisJasaModel extends Model {
    protected $table = 'tbl_jenis_jasa';


    protected $fillable = ['type_jasa','estimasi','estimasi_desc','harga_kiloan','harga_satuan','desc'];

    public $timestamps = false;

    public $incrementing = false;

    public function get_jasa(){
        $query = DB::table($this->table)->select('*');
        $result = $query->get();
        return $result;        
    }

} 