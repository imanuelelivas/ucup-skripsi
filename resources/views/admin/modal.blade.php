<div class="modal modal-blur fade" id="modal-success" tabindex="-1" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         <div class="modal-status bg-success"></div>
         <div class="modal-body text-center py-4">
            <!-- Download SVG icon from http://tabler-icons.io/i/circle-check -->
            <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-green icon-lg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
               <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
               <circle cx="12" cy="12" r="9"></circle>
               <path d="M9 12l2 2l4 -4"></path>
            </svg>
            <h3>Sukses</h3>
            <div class="text-muted" id="message-success">sukses</div>
         </div>
         <div class="modal-footer">
            <div class="w-100">
               <div class="row">
                  <div class="col">
                     <a href="#" class="btn btn-success w-100" data-bs-dismiss="modal" id="ok-success">
                        OK
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- modal gagal -->
<div class="modal modal-blur fade" id="modal-danger" tabindex="-1" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         <div class="modal-status bg-yellow"></div>
         <div class="modal-body text-center py-4">
            <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
            <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-yellow icon-lg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
               <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
               <path d="M12 9v2m0 4v.01"></path>
               <path d="M5 19h14a2 2 0 0 0 1.84 -2.75l-7.1 -12.25a2 2 0 0 0 -3.5 0l-7.1 12.25a2 2 0 0 0 1.75 2.75"></path>
            </svg>
            <h3 id="title-failed">Gagal</h3>
            <div class="text-muted" id="message-failed">gagal</div>
         </div>
         <div class="modal-footer">
            <div class="w-100">
               <div class="row">
                  <div class="col">
                     <a href="#" class="btn btn-yellow w-100" id="ok-failed" data-bs-dismiss="modal">
                        Ok
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- modal suskes tambah -->
<div class="modal modal-blur fade" id="modal-success-add" tabindex="-1" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         <div class="modal-status bg-success"></div>
         <div class="modal-body text-center py-4">
            <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-green icon-lg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
               <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
               <circle cx="12" cy="12" r="9"></circle>
               <path d="M9 12l2 2l4 -4"></path>
            </svg>
            <h3>{{ Session::get('flash_title') }}</h3>
            <div class="text-muted" id="message-success">{{ Session::get('flash_message') }}</div>
         </div>
         <div class="modal-footer">
            <div class="w-100">
               <div class="row">
                  <div class="col">
                     <a href="#" class="btn btn-success w-100" data-bs-dismiss="modal" id="ok-success">
                        OK
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   $('#ok-success').click(function() {
    location.reload();
   });
   
   $('#ok-failed').click(function() {
    location.reload();
   });
</script>
