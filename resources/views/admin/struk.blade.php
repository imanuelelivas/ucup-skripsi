<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head> 
<div align="center" ><a href="javascript:self.close();">[Keluar]</a></div>
<body>
<div align="center" style="font-size: 15px;font-family: Courier New;" id="printDiv">
        <font face="Courier New"/>
            <center>
                <table width="300px" style="background-color: white;">
                <tr>
                    <td colspan="3" style="font-size: 18px;text-align: center;font-weight: bold;">
                        LAUNDRYKU
                    </td>
                </tr> 
                <tr>
                    <td colspan="3" style="font-size: 15px;text-align: center;">
                        <p>Jl. Karbela no. 16 A, Karet kunigan Setiabudi Jakarta Selatan</p>
                        <p>Tlp./WA 0823141461790 <u>www.laundryku.com</u></p>
                    </td>
                </tr>
                <td colspan="3">==============================</td>

                <tr>
                    <td style="font-size: 15px;">No. Transaksi</td>
                    <td>:</td>
                    <td style="font-size: 15px;text-align: right;font-weight: bold;">{{$kode}}</td>
                </tr>
                <tr>
                    <td style="font-size: 15px;">Pelanggan</td>
                    <td>:</td>
                    <td style="font-size: 15px;text-align: right;">{{$nama}}</td>
                </tr>
                <tr>
                    <td style="font-size: 15px;">No. Tlp</td>
                    <td>:</td>
                    <td style="font-size: 15px;text-align: right;">{{$no_tlp}}</td>
                </tr>
                <tr>
                    <td style="font-size: 15px;">Alamat</td>
                    <td>:</td>
                    <td style="font-size: 15px;text-align: right;">{{$alamat}}</td>
                </tr>
                <tr>
                    <td style="font-size: 15px;">Status Bayar</td>
                    <td>:</td>
                    <td style="font-size: 15px;text-align: right;font-weight: bold;">{{strtoupper($status_pembayaran)}}</td>
                </tr>
                <tr>
                    <td style="font-size: 15px;">Tgl. Terima</td>
                    <td>:</td>
                    <td style="font-size: 15px;text-align: right;">{{Mapping::format_tanggal($tgl_transaksi)}}</td>
                </tr>
                <tr>
                    <td style="font-size: 15px;">Tgl. Selesai</td>
                    <td>:</td>
                    <td style="font-size: 15px;text-align: right;">{{Mapping::format_tanggal($tgl_selesai)}}</td>
                </tr>
                <td colspan="3">==============================</td>
                <tr>
                    <td colspan="3" style="font-size: 15px;">@Transaksi</td>
                </tr>
                <tr>
                    <td style="font-size: 15px;">Layanan</td>
                    <td>:</td>
                    <td style="font-size: 15px;text-align: right;font-weight: bold;">{{strtoupper($estimasi_desc)}}</td>
                </tr>
                <tr>
                    <td style="font-size: 15px;">Laundry</td>
                    <td>:</td>
                    <td style="font-size: 15px;text-align: right;font-weight: bold;">{{strtoupper($jenis_cucian_desc)}}</td>
                </tr>
                <td colspan="3">==============================</td>
                <tr>
                    <td style="font-size: 15px;">items</td>
                    <td style="font-size: 15px;">qty</td>
                    <td style="font-size: 15px;text-align: right;">harga(Rp)</td>
                </tr>
                <td colspan="3">------------------------------</td>
                @if($jenis_cucian == '1')
                <tr>
                    <td style="font-size: 15px;">{{$jenis_cucian_desc}}</td>
                    <td style="font-size: 15px;">{{$berat}} Kg</td>
                    <td style="font-size: 15px;text-align: right;">{{$harga}}</td>
                </tr>   
                @else
                @foreach ($items as $rs)
                <tr>
                    <td style="font-size: 15px;">{{$rs->item}}</td>
                    <td style="font-size: 15px;">{{$rs->qty}}</td>
                    <td style="font-size: 15px;text-align: right;">{{$rs->total}}</td>
                </tr>
                @endforeach
                @endif
                <tr>
                    @if($jenis_cucian == '1')
                    <td style="font-size: 15px;" colspan="2">Layanan {{' '.$estimasi_desc.'@'.Mapping::estimasi_harga($estimasi)[1]}}</td>
                    <td style="font-size: 15px;text-align: right;"></td>
                    @else
                    <td style="font-size: 15px;" colspan="2">Layanan {{'@'.$estimasi_desc}}</td>
                    <td style="font-size: 15px;text-align: right;">{{Mapping::estimasi_harga($estimasi)[2]}}</td>
                    @endif
                </tr>
                <tr>
                    @if($fee_status == '1')
                    <td style="font-size: 15px;" colspan="2">Denda Telat Ambil {{'@'.$fee/2000}} Hari</td>
                    <td style="font-size: 15px;text-align: right;">{{$fee}}</td>
                    @endif
                </tr>
                <td colspan="3">------------------------------</td>
                <tr>
                    <td style="font-size: 15px;font-weight: bold;" colspan="2">Total</td>
                    <td style="font-size: 15px;text-align: right;font-weight: bold;">Rp. {{$harga+$fee}}</td>
                </tr>
                <tr>
                    <td style="font-size: 15px;text-align: right;padding-top: 15px;" colspan="3">Petugas : {{Session::get('username')}} </td>
                </tr>
                <tr>
                    <td style="font-size: 15px;text-align: right;" colspan="3">Tgl Print : {{date('d-m-Y H:i:s')}} </td>
                </tr>
                <tr>
                    <td style="font-size: 15px;font-weight: bold;" colspan="3"><small><i>* Jika telat Ambil cucian akan kena denda Rp.2000/hari<i><small></td>
                </tr>
                <tr>
                    <td colspan="3" style="font-size: 15px;text-align: center;padding-top: 15px;">TERIMA KASIH ATAS KUNJUNGAN ANDA</td>
                </tr>
                </table>
            </center>
        </div>
    </body>

<script>
$(document).ready(function() {
    function printStruk() {
        var divToPrint = document.getElementById("printDiv");
        newWin = window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }
    printStruk();
});
</script>
</html>