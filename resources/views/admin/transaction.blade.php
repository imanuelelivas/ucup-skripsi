<!doctype html>
<html lang="en">
  <head>
    @include('layout.head')

  </head>
  <body class="antialiased">
    <div class="wrapper">

      @include('layout.header')

      <div class="page-wrapper">
        <div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  LaundryKu
                </div>
                <h2 class="page-title">
                  Ambil Cucian
                </h2>
              </div>
              <div class="col-auto ms-auto d-print-none">
                <div class="btn-list">
                  <a href="{{ url('area/admin/form_order') }}" class="btn btn-primary d-none d-sm-inline-block">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                    Tambah Cucian
                  </a>
                  <a href="{{ url('area/admin/form_order') }}" class="btn btn-primary d-sm-none btn-icon" aria-label="Tambah Cucian">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="container-xl">
            <div class="row row-deck row-cards">
              <!-- disini -->
              <div class="col-md-12 col-lg-12">
                <div class="card">
                <div class="card-status-top bg-red"></div>
                  <div class="card-header">
                    <h3 class="card-title">Data Cucian Selesai</h3>
                  </div>
                  <form>
                    <div class="card-body border-bottom py-3">
                      <div class="d-flex">
                      <div class="ms-auto text-muted">
                          <div class="ms-2 d-inline-block">
                            <select class="form-select" name="type">
                              <option value="1" {{ request()->get('type') === '1' ? 'selected' : '' }}>Kode Transaksi</option>
                              <option value="2" {{ request()->get('type') === '2' ? 'selected' : '' }}>Nama</option>
                              <option value="3" {{ request()->get('type') === '3' ? 'selected' : '' }}>No. Handphone</option>
                            </select>
                          </div>
                          <div class="ms-2 d-inline-block">
                            <input type="text" class="form-control" aria-label="Search invoice" name="query" placeholder="Search" value=" {{ request()->get('query') }}"> 
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  <div class="card-table table-responsive">
                    <table class="table card-table table-vcenter">
                      <thead>
                          <tr>
                            <th>Tgl. Transaksi</th>
                            <th>Kode Transaksi</th>
                            <th>Nama Pelanggan</th>
                            <th>Harga</th>
                            <th>Pembayaran</th>
                            <th>tanggal selesai</th>
                            <th></th>
                          </tr>
                      </thead>
                      @if ($data_order->count() <= 0)
                        <tr>
                          <td colspan="7">Cucian Selesai tidak ada</td>
                        </tr>
                      @endif
                      @foreach ($data_order as $order)
                          <tr>
                            <td>
                              <?php
                                $date=date_create($order->tgl_transaksi);
                                echo date_format($date,"d/m/Y H:i:s");
                              ?>
                            </td>
                            <td><a href="#" data-id="{{ $order->id}}" id="detailOId" >{{$order->id_transaksi}}</a>
                            <div class="text-muted">{{Mapping::mapping_estimasi($order->type_jasa)}}</div>
                            </td>
                            <td>{{$order->nama_costumer}}</td>
                            <td>
                              <div>Rp {{number_format($order->harga)}}
                                <?php
                                  echo "<input type='hidden' id='hrg_".$order->id."' value='".$order->harga."'>";
                                ?>
                              </div>
                              
                            </td>
                            <td class="w-20">
                            @if($order->status_pembayaran == '1') 
                              Lunas
                            @else
                              Belum dibayar
                            @endif
                            </td>
                            <td>
                            <?php
                                $date_end = date_create($order->tgl_selesai);
                                echo date_format($date_end,"d/m/Y H:i:s");
                              ?>
                              <div class="text-muted">
                                    <?php
                                    if(date('Y-m-d') < date('Y-m-d', strtotime($order->tgl_selesai)) || date('Y-m-d') == date('Y-m-d', strtotime($order->tgl_selesai))){
                                      echo "<input type='hidden' id='tlt_".$order->id."' value='0'>";
                                    }else{
                                      $selesai  = date_create(date('Y-m-d', strtotime($order->tgl_selesai)));
                                      $hari_ini = date_create();
                                      $diff  = date_diff( $selesai, $hari_ini );
                                      echo "<input type='hidden' id='tlt_".$order->id."' value='".$diff->days."'>";
                                        echo "Telat Ambil (".$diff->days." Hari)";
                                    }
                                    ?>
                              </div>
                            </td>
                            <td class="text-end">
                              <div class="form-selectgroup">
                              @if($order->status_pembayaran == '0')
                                <a href="#" class="btn btn-md btn-orange" data-id="{{ $order->id}}" id="bayarDo">
                                      Bayar
                                  </a>
                              @elseif($order->status_pembayaran == '1' && date('Y-m-d') > date('Y-m-d', strtotime($order->tgl_selesai)) && $order->fee_status == '0')
                                <a href="#" class="btn btn-md btn-orange" data-id="{{ $order->id}}" id="bayarDenda">
                                    Bayar Denda
                                </a>
                              @else
                              <a href="#" class="btn btn-md btn-lime" data-id="{{ $order->id}}" id="ambilCucian">
                                    cucian diambil
                                </a>
                              @endif
                              </div>
                            </td>
                          </tr>
                        @endforeach
                    </table>
                    <br>
						          {{ $data_order->links() }}
                  </div>
                </div>
              </div>
          </div>
        </div>
         <!-- modal -->
         @include('admin.modal_detail_done')
				<!-- end modal -->
        @include('admin.modal')
        @include('admin.modal.modal_transaksi')

        @include('layout.footer')
        <!-- modal bayar denda -->
        <div class="modal modal-blur fade" id="modal_fee" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="title">Bayar denda telat ambil</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="byr_fee">
                        @csrf
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Telat Ambil</label>
                                  <input type="text" class="form-control" name="deadline" id="deadline" autocomplete="off" value="" required />
                                </div>
                              </div>
                            </div>
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Total Denda</label>
                                  <input type="text" class="form-control" name="fee" id="fee" autocomplete="off" value="" required />
                                  <input type="hidden" class="form-control" name="id_edt_fee" id="id_edt_fee" required />
                                </div>
                              </div>
                            </div>
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Status Pembayaran</label>
                                    <select class="form-select" id="sts_byr_fee">
                                      <option value="">-Pilih-</option>
                                      <option value="1">Lunas</option>
                                    </select>
                                </div>
                              </div>
                            </div>
                            <i class="text-red">* telat ambil cucian denda Rp 2.000/hari</i>
                            </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                            </a>
                            <div class="ms-auto">
                                <button type="button" class="btn btn btn-primary btn-sm" id="btn-bayar">Bayar
                                </button>
                            </div>
                        </div>
                        </form>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <!-- modal bayar -->
        <div class="modal modal-blur fade" id="modal_bayar" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="title">Form Pembayaran</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="byr_do">
                        @csrf
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Harga Cucian</label>
                                    <input type="text" class="form-control" id="hrg_cucian" autocomplete="off" required>
                                </div>
                              </div>
                        </div>
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Telat Ambil</label>
                                    <input type="text" class="form-control" id="tlt_byr" autocomplete="off" required>
                                </div>
                              </div>
                            </div>
                        <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Denda Telat Ambil</label>
                                    <input type="text" class="form-control" autocomplete="off" id="tlt_fee_byr">
                                  <input type="hidden" class="form-control" name="id_edt_fee_byr" id="id_edt_fee_byr" required/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Total Pembayaran</label>
                                  <div class="input-group mb-2">
                                    <span class="input-group-text">
                                      Rp
                                    </span>
                                    <input type="text" class="form-control" id="ttl_byr" autocomplete="off" required>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col">
                                <div class="mb-3">
                                  <label class="form-label">Status Pembayaran</label>
                                    <select class="form-select" id="sts_byr">
                                      <option value="">-Pilih-</option>
                                      <option value="1">Lunas</option>
                                    </select>
                                </div>
                              </div>
                            </div>
                            <i class="text-red">* telat ambil cucian denda Rp 2.000/hari</i>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                            Cancel
                            </a>
                            <div class="ms-auto">
                                <button type="button" class="btn btn btn-primary btn-sm" id="btn-bayar-do">Bayar
                                </button>
                            </div>
                        </div>
                        </form>
                </div>
            </div>
        </div>
        <!-- end modal -->
      </div>
    </div>
    <script src="{{asset('dist/js/tabler.min.js')}}"></script>
    <script>
       $("body").on("click", "#bayarDo", function (event) {
        event.preventDefault();
        // declare variable
        var id = $(this).data("id");
        var tlt = $('#tlt_'+id).val();
        var hrg = $('#hrg_'+id).val();
        var denda = parseInt(tlt) * 2000;
        var harga = parseInt(hrg) + parseInt(denda);
        // set value
        $('#tlt_byr').val(tlt+" hari");
        $('#hrg_cucian').val(hrg);
        $('#id_edt_fee_byr').val(id);
        $('#tlt_fee_byr').val(formatNumber(denda));
        $('#ttl_byr').val(formatNumber(harga));
        //readonly
        $('#deadline').prop('readonly', true);
        $('#tlt_fee_byr').prop('readonly', true);
        $('#tlt_byr').prop('readonly', true);
        $('#hrg_cucian').prop('readonly', true);
        $('#ttl_byr').prop('readonly', true);
        //show modal
        $("#modal_bayar").modal("show");
        
        // if click bayar
        $("#btn-bayar-do").click(function(){
          if($('#sts_byr').val() == ""){
            alert("Status Pembayaran harus dipilih");
            $('#sts_byr').focus();
            return false;
          }
           let _url = "{{ url('area/admin/denda') }}";
            $.ajax({
                url: _url,
                type: "POST",
                data: {
                  telat: tlt,
                  id: id,
                  _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if(response.status == "00") {
                      $("#modal_bayar").modal("hide");
                      $("#message-success").html(response.message);
                      $('#modal-success').modal('show');
                    }else{
                      $("#modal_bayar").modal("hide");
                      $("#message-failed").html(response.message);
                      $("#title-failed").html(response.title);
                      $('#modal-danger').modal('show');
                    }
                },
                error: function(response) {
                  $("#modal_bayar").modal("hide");
                  $("#message-failed").html(response);
                  ('#modal-danger').modal('show');
                }
            });
        })
   });

   // Bayar Denda
   $("body").on("click", "#bayarDenda", function (event) {
        event.preventDefault();
        var id = $(this).data("id");
        var tlt = $('#tlt_'+id).val();
        $('#deadline').val(tlt+" hari");
        $('#id_edt_fee').val(id);
        var denda = parseInt(tlt) * 2000;
        $('#fee').val(formatNumber("Rp. "+ denda));
        $('#deadline').prop('readonly', true);
        $('#fee').prop('readonly', true);
        $("#modal_fee").modal("show");
        // if click bayar
        $("#btn-bayar").click(function(){
          if($('#sts_byr_fee').val() == ""){
            alert("Status Pembayaran harus dipilih");
            $('#sts_byr_fee').focus();
            return false;
          }
           let _url = "{{ url('area/admin/denda') }}";
            $.ajax({
                url: _url,
                type: "POST",
                data: {
                  telat: tlt,
                  id: id,
                  _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if(response.status == "00") {
                      $("#modal_fee").modal("hide");
                      $("#message-success").html(response.message);
                      $('#modal-success').modal('show');
                    }else{
                      $("#modal_fee").modal("hide");
                      $("#message-failed").html(response.message);
                      $("#title-failed").html(response.title);
                      $('#modal-danger').modal('show');
                    }
                },
                error: function(response) {
                  $("#modal_fee").modal("hide");
                  $("#message-failed").html(response);
                  ('#modal-danger').modal('show');
                }
            });
        })
   });


     // Bayar Denda
     $("body").on("click", "#ambilCucian", function (event) {
        event.preventDefault();
        var id = $(this).data("id");
           let _url = "{{ url('area/admin/ambil') }}";
            $.ajax({
                url: _url,
                type: "POST",
                data: {
                  id: id,
                  _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    if(response.status == "00") {
                      $("#modal_fee").modal("hide");
                      $("#message-success").html(response.message);
                      $('#modal-success').modal('show');
                      var url = 'https://localhost/laundry/public/area/admin/struk/'+response.kode+'/'+response.name;
                      window.open(url, '_blank');
                    }else{
                      $("#modal_fee").modal("hide");
                      $("#message-failed").html(response.message);
                      $("#title-failed").html(response.title);
                      $('#modal-danger').modal('show');
                    }
                },
                error: function(response) {
                  $("#modal_fee").modal("hide");
                  $("#message-failed").html(response);
                  ('#modal-danger').modal('show');
                }
            });
     
   });

   function formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }  
    </script>
  </body>
</html>