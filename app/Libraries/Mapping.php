<?php 

namespace App\Libraries;

use App\Models\TblJenisJasaModel;
use App\Models\TblJenisCucianModel;
use App\Models\TblStatusModel;

class Mapping {

    //initializing values in this constructor
    function __construct() { 
    }
    
    public static function mapping_jenis_cucian($id) {
        $jenis_cucian = TblJenisCucianModel::where('id',$id)->first();
        if($jenis_cucian == null){
          return '-';
        }
        return $jenis_cucian->type; 

    }

    public static function mapping_estimasi($id) {
      $estimasi = TblJenisJasaModel::where('id',$id)->first();
      if($estimasi == null){
        return '-';
      }
      return $estimasi->type_jasa; 

    } 

    public static function mapping_estimasi_desc($id) {
      $estimasi = TblJenisJasaModel::where('id',$id)->first();
      if($estimasi == null){
        return '-';
      }
      if($estimasi->estimasi_desc == 'day') {
        return ' Hari';

      }else{
        return ' Jam'; 
        
      }

      return ' '; 

    } 
    
    public static function estimasi_harga($id) {
      $estimasi = TblJenisJasaModel::where('id',$id)->first();
      if($estimasi == null){
        return array("-","","");
      }
      return array($estimasi->type_jasa,$estimasi->harga_kiloan,$estimasi->harga_satuan);

    }

    public static function mapping_pembayaran($status) {
      $status_pembayaran = TblStatusModel::where('group','pembayaran')->where('status',$status)->first();
      if($status_pembayaran == null){
        return array("-","");
      }
      return array($status_pembayaran->status_desc,$status_pembayaran->style_desc); 
    }

    public static function mapping_status_cucian($status) {
      $status_cucian = TblStatusModel::where('group','cucian')->where('status',$status)->first();
      if($status_cucian == null){
        return array("-","");
      }
      return array($status_cucian->status_desc,$status_cucian->style_desc); 
    }

    public static function format_tanggal($tanggal){
      $date = date_create($tanggal);
      return date_format($date,"d/m/Y H:i:s");
    }
}