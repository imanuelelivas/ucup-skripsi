<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use Excel;
use App\Models\TblTrxModel;
use App\Exports\DailyExport;
use App\Libraries\Mapping;
use Illuminate\Support\Facades\Log;

class ReportDailyController extends Controller
{
	function index(Request $request){
        if($request->get("start") != "" && $request->get("end") != "") {
            $dataTrx = TblTrxModel::whereDate('tgl_transaksi','>=',$request->get("start"))->whereDate('tgl_transaksi','<=',$request->get("end"));
          }else{
            $dataTrx = TblTrxModel::whereDate('tgl_transaksi','>=',date('Y-m-d'));
          }
          $pemasukan = $dataTrx->sum('harga');
          $fee = $dataTrx->sum('fee');
          $report = $dataTrx->orderBy('tgl_transaksi','ASC')->paginate(10);
        $data = array(
            'active_laporan'    => "active",
            'data_harian'       => $report,
            'pemasukan'         => $pemasukan,
            'fee'               => $fee,
            'start'               => $request->get("start") ?? date('Y-m-d'),
            'end'               => $request->get("end") ?? date('Y-m-d'),
            );
            return View('admin.report.report_daily')->with($data);
	}

  public function exportExcel(Request $request) {
    $nama_file = 'laporan_harian_laundryku_'.date('Y-m-d_H-i-s').'.xlsx';
    return Excel::download(new DailyExport, $nama_file);
  }

}
