<?php

namespace App\Exports;

use App\Models\TblTrxModel;
use App\Libraries\Mapping;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class DailyExport implements FromView
{
    public function view(): View
    {
        $start = request()->input('start');
        $end = request()->input('end');
        if($start != "" && $end != "") {
            $dataTrx = TblTrxModel::whereDate('tgl_transaksi', '>=', $start)->whereDate('tgl_transaksi', '<=', $end);
        }else{
            $dataTrx = TblTrxModel::whereDate('tgl_transaksi', '>=', date('Y-m-d'));
        }
        $pemasukan = $dataTrx->sum('harga');
        $fee = $dataTrx->sum('fee');
        return view('admin.report.report_daily_excel', [
            'data_harian' => $dataTrx->get(),
            'pemasukan' => $pemasukan,
            'fee' => $fee,
            'start' => $start,
            'end' => $end,
        ]);
    }
}