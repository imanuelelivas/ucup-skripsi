<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.head')
    <style>
        .select2-container {
          width: 100% !important;
        }
    </style>
<script type="text/javascript">
$(document).ready(function() {
      $('.cari').select2({
        minimumInputLength: 1,
        placeholder: 'Cari pakaian...',
        ajax: {
          url: "{{ url('area/admin/autocomplate') }}",
          dataType: 'json',
          delay: 100,
          processResults: function (data) {
            return {
              results:  $.map(data, function (item) {
                return {
                  text: item.jenis_pakaian+"  ("+item.harga_satuan+"/pcs)",
                  id: item.harga_satuan
                }
              })
            };
          },
        }
      });
      });
</script>
  </head>
  <body class="antialiased">
    <div class="wrapper">
      @include('layout.header')
      <div class="page-wrapper">
        <div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  LaundryKu
                </div>
                <h2 class="page-title">
                  Orderan Masuk
                </h2>
              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="container-xl">
            <div class="row row-deck row-cards">
            <div class="col-md-2"></div>
              <div class="col-md-8">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Tambah Cucian</h3>
                  </div>
                  <div class="card-body">
                  @if(Session::has('flash_message'))
                  <div class="alert alert-danger alert-dismissible" role="alert">
                <div class="d-flex">
                  <div>
                  </div>
                  <div>
                    <h4 class="alert-title" id="alert-title">{{ Session::get('flash_title') }}&hellip;</h4>
                    <div class="text-muted" id="alert-body">{{ Session::get('flash_message') }}</div>
                  </div>
                </div>
                <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
            </div>
            @endif
                  <form class="add-listing-form" action="{{ url('area/admin/add_order') }}" method="post" id="tambah">
                    @csrf
                      <div class="row">
                          <div class="col-lg-8">
                            <div class="mb-3">
                              <label class="form-label">Nama</label>
                              <input type="text" class="form-control" name="nama" id="nama" placeholder="Ex. Alkalin" autocomplete="off"  required />
                            </div>
                          </div>
                          <div class="col-lg-4">
                            <div class="mb-3">
                              <label class="form-label">No. Telpon</label>
                              <input type="text" class="form-control" name="telpon" placeholder="ex. 08231452765" maxlength="13" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required />
                            </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-lg-12">
                            <div class="mb-3">
                              <label class="form-label">Alamat</label>
                              <textarea class="form-control" name="alamat" id="alamat" rows="2" placeholder="ex. Apartemen Green Bamboo no.12"></textarea>
                            </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-lg-8">
                            <div class="mb-3">
                              <label class="form-label">Jenis Laundry</label>
                              <select class="form-select" name="jenis_cucian" id="jenis_cucian" required>
                                @foreach ($jenis_cucian as $jenis) 
                                  <option value="{{$jenis->id}}">{{$jenis->type}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-4" id="id_berat">
                            <div class="mb-3">
                              <label class="form-label">Berat(Kg)</label>
                              <input type="text" class="form-control" name="berat" id="berat" placeholder="Berat Cucian (Kg)" autocomplete="off" oninput="this.value=this.value.replace(/[^0-9.]/g,'');" />
                            </div>
                          </div>
                      </div>
                      <input type="hidden" class="form-control" id="js_price" value="0"/>
                      <div class="row" id="form-satuan">
                          <div class="col-lg-6">
                            <div class="mb-3">
                            <label class="form-label">Jenis Pakaian</label>
                            <select class="cari form-select" name="cari" id="pakaian"></select>
                            </div>
                          </div>
                          <div class="col-lg-2">
                            <div class="mb-3">
                            <label class="form-label">jumlah/pcs</label>
                            <input type="text" class="form-control" name="pcs" id="pcs" placeholder="" onkeyup="this.value=this.value.replace(/[^\d]/,'')" />
                            </div>
                          </div>
                          <div class="col-lg-2">
                            <div class="mb-3">
                            <label class="form-label">&nbsp;</label>
                            <button type="button" class="btn btn-primary ms-auto" id="btn-satuan" onclick="tambahPakaian(); return false;">Tambah Pakaian</button>
                            </div>
                          </div>
                      </div>
                      <input id="idf" value="1" type="hidden" />
                      <div id="divHobi"></div>
                      <label class="form-label">Estimasi Pengerjaan</label>
                      <div class="form-selectgroup form-selectgroup-boxes row mb-3">
                          @foreach ($estimasi as $js) 
                              <div class="col-lg-4">
                                <label class="form-selectgroup-item">
                                  <input type="hidden" class="form-control" id="harga_{{$js->id}}" value="{{$js->harga_kiloan}}"/>
                                  <input type="hidden" class="form-control" id="harga_s{{$js->id}}" value="{{$js->harga_satuan}}"/>
                                  <input type="radio" name="estimasi" value="{{$js->id}}" class="form-selectgroup-input" required/>
                                  <span class="form-selectgroup-label d-flex align-items-center p-3">
                                    <span class="me-3">
                                      <span class="form-selectgroup-check"></span>
                                    </span>
                                    <span class="form-selectgroup-label-content">
                                      <span class="form-selectgroup-title strong mb-1">{{$js->type_jasa}}</span>
                                      <span class="d-block text-muted">{{$js->desc}}</span>
                                    </span>
                                  </span>
                                </label>
                              </div>
                          @endforeach
                      </div>
                      <div class="row">
                          <div class="col-lg-8">
                            <div class="mb-3">
                              <label class="form-label">Pembayaran</label>
                              <select class="form-select" name="pembayaran" id="pembayaran" required>
                              <option value="">Pilih</option>
                              <option value="0">Bayar Nanti</option>
                              <option value="1">Bayar Sekarang (Lunas)</option>
                              </select>
                            </div>
                          </div>
                      </div>
                      <div class="d-flex">
                      <div class="display-6 my-3 ms-auto" id="harga_js">Rp 0</div>
                      </div>
                      <div class="form-footer">
                        <div class="d-flex">
                          <button type="submit" class="btn btn-primary ms-auto">Buat Orderan</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <footer class="footer footer-transparent d-print-none">
          <div class="container">
            <div class="row text-center align-items-center flex-row-reverse">
              <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                <ul class="list-inline list-inline-dots mb-0">
                  <li class="list-inline-item">
                    Copyright &copy; 2021
                    <a href="." class="link-secondary">LaundyKu</a>. All rights reserved.
                  </li>
                  <li class="list-inline-item">
                    <a href="./changelog.html" class="link-secondary" rel="noopener">v0.0.1 Dev</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <script src="{{asset('dist/js/tabler.min.js')}}"></script>
    <script type="text/javascript">
      $(document).ready(function(){
      $('#form-satuan').hide();
      if($("#jenis_cucian").val() == "1"){
          $('#id_berat').show();
          $('#form-satuan').hide();
          $('input:radio[name="estimasi"]').change(
            function(){
                if (this.checked) {
                  if($("#jenis_cucian :selected").val() == "1"){
                    if($("#berat").val() == ""){
                      $("#harga_js").html("Rp "+formatNumber(0));
                      alert("berat harus diisi");
                      $("#berat").focus();
                      return false;
                    }
                    else if(parseFloat($("#berat").val()) < 2){
                      $("#berat").val("");
                      $("#harga_js").html("Rp "+formatNumber(0));
                      alert("minimal berat cucian 2 Kg");
                      return false;
                    }
                    id_harga = "#harga_"+$("input[name='estimasi']:checked").val();
                    valuee = parseFloat($("#berat").val()) * parseInt($(id_harga).val());
                    harga = Math.ceil(valuee);
                    console.log(harga);
                    $("#harga_js").html("Rp "+formatNumber(harga));
                  }
                }
          });
          $('input[name=berat]').change(
              function() {
                if (!$("input[name='estimasi']:checked").val()) {
                    // $("#berat").val("");
                    $("#harga_js").html("Rp "+formatNumber(0));
                  // alert('silakan pilih estimasi pengerjaan terlebih dahulu');
                  //   return false;
                }
                else if(parseFloat($("#berat").val()) < 2){
                      $("#berat").val("");
                      $("#harga_js").html("Rp "+formatNumber(0));
                      alert("minimal berat cucian 2 Kg");
                      return false;
                    }
                else {
                    id_harga = "#harga_"+$("input[name='estimasi']:checked").val();
                    valuee = parseFloat($("#berat").val()) * parseInt($(id_harga).val());
                    harga = Math.ceil(valuee);
                    console.log(harga);
                    $("#harga_js").html("Rp "+formatNumber(harga));
                }
            });
        }
      $('#jenis_cucian').change(function() {
        if($("#jenis_cucian :selected").val() == "1"){
          $('#id_berat').show();
          $('#form-satuan').hide();
          $('input:radio[name="estimasi"]').change(
            function(){
                if (this.checked) {
                  if($("#jenis_cucian :selected").val() == "1"){
                    if($("#berat").val() == ""){
                      $("#harga_js").html("Rp "+formatNumber(0));
                      alert("berat harus diisi");
                      $("#berat").focus();
                      return false;
                    }
                    else if(parseFloat($("#berat").val()) < 2){
                      $("#berat").val("");
                      $("#harga_js").html("Rp "+formatNumber(0));
                      alert("minimal berat cucian 2 Kg");
                      return false;
                    }
                    id_harga = "#harga_"+$("input[name='estimasi']:checked").val();
                    valuee = parseFloat($("#berat").val()) * parseInt($(id_harga).val());
                    harga = Math.ceil(valuee);
                    console.log(harga);
                    $("#harga_js").html("Rp "+formatNumber(harga));
                  }
                }
          });
          $('input[name=berat]').change(
              function() {
                if (!$("input[name='estimasi']:checked").val()) {
                    // $("#berat").val("");
                    $("#harga_js").html("Rp "+formatNumber(0));
                  // alert('silakan pilih estimasi pengerjaan terlebih dahulu');
                  //   return false;
                }
                else if(parseFloat($("#berat").val()) < 2){
                      $("#berat").val("");
                      $("#harga_js").html("Rp "+formatNumber(0));
                      alert("minimal berat cucian 2 Kg");
                      return false;
                    }
                else {
                    id_harga = "#harga_"+$("input[name='estimasi']:checked").val();
                    valuee = parseFloat($("#berat").val()) * parseInt($(id_harga).val());
                    harga = Math.ceil(valuee);
                    console.log(harga);
                    $("#harga_js").html("Rp "+formatNumber(harga));
                }
            });
        }else if($("#jenis_cucian :selected").val() == "2"){
          $('#id_berat').hide();
          $('#form-satuan').show();
          //script untuk ganti2 estimasi pengeraan
          $('input:radio[name="estimasi"]').change(
            function(){
                if (this.checked) {
                  if($("#jenis_cucian :selected").val() == "2"){
                      var id_harga = "#harga_s"+$("input[name='estimasi']:checked").val();
                      var js_price = $('#js_price').val();
                      var hargaku =  parseInt(js_price) + parseInt($(id_harga).val());
                      var harga_js = $('#harga_js').html("Rp "+formatNumber(hargaku));
                      // $('#js_price').val(hargaku);
                  }
                }
          });
        }else{
          alert('Jenis Cucian harus dipilih');
          return false;
        }
      });
    });
    function formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }    

    function tambahPakaian() {
      var pcs = $('#pcs').val();
      var pakaian = $('#pakaian').val();
      var pakaian_teks = $('#pakaian').text();
      var idf = document.getElementById("idf").value;
      if(pakaian_teks == ""){
        $('#pakaian').focus();
        alert("jenis pakaian harus dipilih");
        return false;
      }
      if(pcs == ""){
        $('#pcs').focus();
        alert("jumlah harus diisi");
        return false;
      }
      if(parseInt(pcs) <= 0){
        $('#pcs').val('');
        $('#pcs').focus();
        alert("jumlah harus diatas 0");
        return false;
      }
      if (Number.isNaN(parseInt(pcs)) == true){
        $('#pcs').val('');
        $('#pcs').focus();
        alert("Jumlah harus angka");
        return false;
      }
      if (!Number.isInteger(parseFloat(pcs))){
        $('#pcs').val('');
        $('#pcs').focus();
        alert("Jumlah harus angka bulat");
        return false;
      }
      var stre;
      stre = "<div class='row' id='srow_"+idf+"'><div class='col-lg-6'><div class='mb-3'><input type='text' class='form-control' name='jsatuan[]' id='jsatuan"+idf+"' readonly/></div></div><div class='col-lg-2'><div class='mb-3'> <input type='text' class='form-control' name='jpcs[]' id='jpcs_"+idf+"' readonly/> <input type='hidden' class='form-control' name='jharga[]' id='jharga_"+idf+"'  readonly/></div></div><div class='col-6 col-sm-4 col-md-2 col-xl-auto mb-3'> <a href='#' class='btn w-100 btn-icon' onclick='hapusElemen(\"#srow_"+idf+"\"); return false;'> <svg xmlns='http://www.w3.org/2000/svg' class='icon' width='24' height='24' viewBox='0 0 24 24' stroke-width='2' stroke='currentColor' fill='none' stroke-linecap='round' stroke-linejoin='round'><path stroke='none' d='M0 0h24v24H0z' fill='none'/><line x1='4' y1='7' x2='20' y2='7' /><line x1='10' y1='11' x2='10' y2='17' /><line x1='14' y1='11' x2='14' y2='17' /><path d='M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12' /><path d='M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3' /></svg> </a></div></div>";
      $("#divHobi").append(stre);
      // alert("#jsatuan"+idf+" = "+pakaian);
      $("#jsatuan"+idf).val(pakaian_teks);
      $("#jpcs_"+idf).val(parseInt(pcs));
      $("#jharga_"+idf).val(pakaian);
      $( ".cari" ).val('').trigger('change');
      $( ".cari" ).text('').trigger('change');
      idf = (idf-1) + 2;
      document.getElementById("idf").value = idf;
      var js_price = $('#js_price').val();
      var hargaku = parseInt(pcs) * parseInt(pakaian) + parseInt(js_price);
      // alert(hargaku)
      // var harga_js = $('#harga_js').html(hargaku);
      $('#js_price').val(hargaku);
      $('#pcs').val("");
      $('#pcs').empty();
      var id_harga = "#harga_s"+$("input[name='estimasi']:checked").val();
      var js_price = $('#js_price').val();
      var harga_before_est =  parseInt(js_price) + parseInt($(id_harga).val());
      $('#harga_js').html("Rp "+formatNumber(harga_before_est));
    }

    function hapusElemen(idf) {
      var id_row = idf;
      var explode = id_row.split("_");
      idff = explode[1];
      var id_harga = "#harga_s"+$("input[name='estimasi']:checked").val();
      // alert("harga_estimasi : "+parseInt($(id_harga).val()))
      var harga_remove = parseInt($("#jpcs_"+idff).val()) * parseInt($("#jharga_"+idff).val());
      // alert("harga_remove : "+harga_remove)
      var harga_now = parseInt($('#js_price').val());
      // alert("harga_now : "+harga_now)
      var hargaku = harga_now - harga_remove;
      // alert("harga_ku : "+hargaku)
      var harga_sebenarnya = parseInt($(id_harga).val()) + hargaku
      // alert("harga_sebenarnya : "+harga_sebenarnya)
      var harga_js = $('#harga_js').html("Rp "+formatNumber(harga_sebenarnya));
      $('#js_price').val(hargaku);
      $(idf).remove();
    }
    </script>
  </body>
</html>
