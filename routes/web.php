<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login',function(){
    if (Session::has('IS_LOGIN')){
        if(Session::get('IS_LOGIN') == "1"){
            return Redirect('area/admin/home');
        }else{
            return view('admin.sign_in');
        }
    }else{
        return view('admin.sign_in');
    }
    
});

Route::get('/',function(){
    return view('welcome');
});

Route::get('/404',function(){
    return view('page_404');
});

// login
Route::post('act/login','SignInController@signIn');

// check status progress by costumer
Route::post('check_state','ProgressStateController@progress');

$router->group(['prefix' => 'area/admin/','middleware'=>['loginCheck']], function ($app) {
    // home
    $app->get('home','HomeController@home');
    //cetak struk
    $app->get('struk/{code}/{costumer}','transaction\StrukController@struk');
    // detail
    $app->get('detail/{id}','HomeController@detail');

    // transaksi
    $app->get('form_order','transaction\AddOrderanController@form');
    $app->get('transaction','transaction\TransactionController@index');
    $app->post('denda','transaction\TransactionController@bayar_fee');
    $app->post('ambil','transaction\TransactionController@ambil_cucian');
    $app->GET('selesai','transaction\TransactionController@done_laundry');
    $app->post('add_order','transaction\AddOrderanController@add');
    $app->get('autocomplate','transaction\AddOrderanController@loadData');
    $app->post('update_payment','transaction\UpdateOrderanController@update_pembayaran');
    $app->post('update_progress','transaction\UpdateOrderanController@update_status_cucian');

    // ganti password
    $app->get('setting',function(){
        return view('admin.setting');
    });
    $app->post('ganti_password','SignInController@password_update');
    //logout
    $app->get('logout','SignInController@signOut');

    //laporan harian
    $app->get('report/daily','transaction\ReportDailyController@index');
    $app->get('report/daily_to_excel','transaction\ReportDailyController@exportExcel');
    //laporan bulanan
    $app->get('report/monthly','transaction\ReportMonthlyController@index');
    $app->get('report/monthly_to_excel','transaction\ReportMonthlyController@exportExcel');
    //laporan tahunan
    $app->get('report/annual','transaction\ReportAnnualController@index');
    $app->get('report/annual_to_excel','transaction\ReportAnnualController@exportExcel');
});

// route only for level user SU
$router->group(['prefix' => 'area/admin/','middleware'=>['loginCheck','privilegeSuperUser']], function ($app) {
    // jenis pakaian
    $app->get('type_unit','master\TypeClothesController@index');
    $app->post('act_type_unit','master\TypeClothesController@act_add_items');
    $app->post('delete_type_unit','master\TypeClothesController@delete');
    $app->post('get_type_unit','master\TypeClothesController@get');
    $app->post('update_type_unit','master\TypeClothesController@update');
    // layanan
    $app->get('service_type','master\ServiceTypeController@index');
    $app->post('act_service_type','master\ServiceTypeController@act_add_items');
    $app->post('get_service_type','master\ServiceTypeController@get');
    $app->post('update_service_type','master\ServiceTypeController@update');
    $app->post('delete_service_type','master\ServiceTypeController@delete');
    
    //petugas
    $app->get('petugas','master\UserController@index');
    $app->post('tambah_petugas','master\UserController@act_add_user');
    $app->post('delete_petugas','master\UserController@delete');
    $app->post('detail_petugas','master\UserController@get');
    $app->post('update_petugas','master\UserController@update_user');

    // delete
    $app->post('delete','transaction\DeleteOrderanController@delete');

    // edit data
    $app->get('edit/{code}','transaction\EditOrderanController@edit');
    $app->post('edit','transaction\EditOrderanController@act_edit');
    $app->post('edit_satuan','transaction\EditOrderanController@act_edit_satuan');
});