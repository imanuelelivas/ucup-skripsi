<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblTrxModel;
use App\Models\TblJenisJasaModel;
use App\Models\TblJenisCucianModel;
use App\Models\TblJenisPakaianModel;
use App\Models\TblLaundrySatuanModel;
use Illuminate\Support\Facades\Log;

class AddOrderanController extends Controller
{
	function form(Request $request){
		$estimasi = TblJenisJasaModel::get();
		$jenis_cucian = TblJenisCucianModel::get();
		$data = array(
		'estimasi'  =>  $estimasi,
		'jenis_cucian'  =>  $jenis_cucian,
		'active_trx'      =>  "active",
		);
		return View('admin.add_order')->with($data);
	}

	function add(Request $request){
		$rules = array(
			'nama'       	=> 'required',
			'telpon'    	=> 'required|numeric',
			'jenis_cucian' 	=> 'required|numeric',
			'berat'    		=> '',
			'estimasi'    	=> 'numeric',
			'alamat'    	=> '',
		);    
		$messages = array(
			'nama.required'       	=> 'Nama wajib diisi',
			'telpon.required'    	=> 'telpon wajib diisi',
			'jenis_cucian.required'	=> 'jenis cucian wajib diisi',
		);
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
		  Session::flash('flash_message', $validator->errors()->first());
		  Session::flash('color_card', 'red');
		  Session::flash('color_text', 'white-text');
		  return Redirect::back()->withInput();
		}
		$kode = $this->generate_kode_trx();
		$estimasi = TblJenisJasaModel::where('id',$request->input('estimasi'))->first();
		if($estimasi == null){
			Session::flash('flash_message', "Estimasi Pengerjaan tidak valid");
			Session::flash('color_card', 'red');
			Session::flash('color_text', 'white-text');
			return Redirect::back()->withInput();
		}
		if($request->input('jenis_cucian') == '1'){
			$est = ' +'.$estimasi->estimasi.' '.$estimasi->estimasi_desc;
			$tgl_trx = date('Y-m-d H:i:s');
			$tgl_done = date('Y-m-d H:i:s', strtotime($tgl_trx . $est));
			$berat = $request->input('berat');
			if($request->input('berat') < 2){
				$berat = 2;
			}
			// harga
			$harga = $berat * $estimasi->harga_kiloan;
			$tgl_bayar = NULL;
			if($request->input('pembayaran') == '1') {
				$tgl_bayar = date('Y-m-d H:i:s');
			}
			$add_order  = TblTrxModel::create([
				'id_transaksi'    	=> $kode,
				'petugas'           => Session::get('username'),
				'nama_costumer'     => $request->input('nama'),
				'alamat'     		=> $request->input('alamat'),
				'nomor_tlp'     	=> $request->input('telpon'),
				'jenis_cucian'  	=> $request->input('jenis_cucian'),
				'type_jasa'        	=> $request->input('estimasi'),
				'berat_cucian'      => $request->input('berat'),
				'tgl_transaksi'     => $tgl_trx,
				'tgl_selesai'       => $tgl_done,
				'harga'        		=> ceil($harga),
				'status_cucian'     => '1',
				'status_pembayaran' => $request->input('pembayaran'),
				'tgl_pembayaran' 	=> $tgl_bayar,
			]);
			if ($add_order) {
				Session::flash('flash_message', "Tambah cucian berhasil");
				Session::flash('flash_title', 'berhasil');
				return Redirect('area/admin/home')->withInput();
			}else{
				Session::flash('flash_message', "Tambah cucian gagal");
				Session::flash('flash_title', 'Gagal');
				return Redirect('area/admin/home')->withInput();
			}
		}else{
			$insert = $this->add_satuan($request,$kode,$estimasi);
			if(!$insert){
				return Redirect::back()->withInput();
			}
			return redirect('area/admin/home');
		}
	}

	function add_satuan($request,$idTrx,$est) {
		$rules = array(
			'jsatuan'   => 'required|array',
			'jpcs'    	=> 'required|array',
			'jharga' 	=> 'required|array',
		);
		$validator = \Validator::make( $request->all(), $rules, []);
		if ($validator->fails()) {
		  	Session::flash('flash_message', "Tambah cucian gagal");
			Session::flash('flash_title', 'Gagal');
			return Redirect('area/admin/home')->withInput();
		}
		$pakaian = $request->input('jsatuan');
		$jpcs = $request->input('jpcs');
		$jharga = $request->input('jharga');
		for($count = 0; $count < count($pakaian); $count++) {
			$data = array(
				'id_transaksi' 	=> $idTrx,
				'item' 			=> $pakaian[$count],
				'qty'  			=> $jpcs[$count],
				'total'  		=> $jpcs[$count]*$jharga[$count],
			);

			$insert_data[] = $data; 
		}
		$addLaudrySatuan = TblLaundrySatuanModel::insert($insert_data);
		if(!$addLaudrySatuan){
			return false;
		}
		$est = ' +'.$est->estimasi.' '.$est->estimasi_desc;
		$tgl_trx = date('Y-m-d H:i:s');
		$tgl_done = date('Y-m-d H:i:s', strtotime($tgl_trx . $est));
		$jsa = TblJenisJasaModel::where('id',$request->input('estimasi'))->first();
		$harga = TblLaundrySatuanModel::select(TblLaundrySatuanModel::raw("SUM(total) as total_harga"))
	    ->where("id_transaksi",$idTrx)->get();
		$add_order  = TblTrxModel::create([
			'id_transaksi'    	=> $idTrx,
			'petugas'           => Session::get('username'),
			'nama_costumer'     => $request->input('nama'),
			'nomor_tlp'     	=> $request->input('telpon'),
			'jenis_cucian'  	=> $request->input('jenis_cucian'),
			'type_jasa'        	=> $request->input('estimasi'),
			'tgl_transaksi'     => $tgl_trx,
			'tgl_selesai'       => $tgl_done,
			'harga'        		=> $harga[0]->total_harga+$jsa->harga_satuan,
			'status_cucian'     => '1',
			'status_pembayaran' => $request->input('pembayaran'),
		]);
		if ($add_order) {
			return true;
		}else{
			return false;
		}
	}

	function generate_kode_trx(){
		$permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		// Output: 54esm
		$acak = substr(str_shuffle($permitted_chars), 0, 5);
		$tgl = date('Ymd');
		return $tgl.$acak; 
	}


	public function loadData(Request $request)
    {
    	if ($request->has('q')) {
    		$cari = $request->q;
    		$data = TblJenisPakaianModel::select('harga_satuan', 'jenis_pakaian')->where('jenis_pakaian', 'LIKE', '%'.$cari.'%')->get();
			Log::info($cari);
			Log::info($data);
    		return response()->json($data);
    	}
    }
}
