<!doctype html>
<html lang="en">
  <head>
    @include('layout.head')

  </head>
  <body class="antialiased">
    <div class="wrapper">

      @include('layout.header')

      <div class="page-wrapper">
        <div class="container-xl">
          <!-- Page title -->
          <div class="page-header d-print-none">
            <div class="row align-items-center">
              <div class="col">
                <!-- Page pre-title -->
                <div class="page-pretitle">
                  LaundryKu
                </div>
                <h2 class="page-title">
                  Home
                </h2>
              </div>
              <!-- Page title actions -->
              <div class="col-auto ms-auto d-print-none">
                <div class="btn-list">
                  <a href="{{ url('area/admin/form_order') }}" class="btn btn-primary d-none d-sm-inline-block">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                    Tambah Cucian
                  </a>
                  <a href="{{ url('area/admin/form_order') }}" class="btn btn-primary d-sm-none btn-icon" aria-label="Tambah Cucian">
                    <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="12" y1="5" x2="12" y2="19" /><line x1="5" y1="12" x2="19" y2="12" /></svg>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="page-body">
          <div class="container-xl">
            <div class="row row-deck row-cards">
              <div class="col-md-6 col-xl-3">
                <div class="card card-sm">
                  <div class="card-body">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <span class="bg-red text-white avatar">
	                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="6" cy="19" r="2" /><circle cx="17" cy="19" r="2" /><path d="M17 17h-11v-14h-2" /><path d="M6 5l6.005 .429m7.138 6.573l-.143 .998h-13" /><path d="M15 6h6m-3 -3v6" /></svg>
                        </span>
                      </div>
                      <div class="col">
                        <div class="font-weight-medium">
                          Cucian diterima hari ini
                        </div>
                        <div class="h1 m-0">{{$cucian_masuk}}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-xl-3">
                <div class="card card-sm">
                  <div class="card-body">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <span class="bg-orange text-white avatar">
	                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="6" cy="19" r="2" /><circle cx="17" cy="19" r="2" /><path d="M17 17h-11v-14h-2" /><path d="M6 5l7.999 .571m5.43 4.43l-.429 2.999h-13" /><path d="M17 3l4 4" /><path d="M21 3l-4 4" /></svg>
                        </span>
                      </div>
                      <div class="col">
                        <div class="font-weight-medium">
                          Cucian belum diproses
                        </div>
                        <div class="h1 m-0">{{$cucian_antri}}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-xl-3">
                <div class="card card-sm">
                  <div class="card-body">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <span class="bg-yellow text-white avatar">
	                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="6" cy="19" r="2" /><circle cx="17" cy="19" r="2" /><path d="M17 17h-11v-14h-2" /><path d="M6 5l14 1l-1 7h-13" /></svg>
                        </span>
                      </div>
                      <div class="col">
                        <div class="font-weight-medium">
                          Cucian sedang diproses
                        </div>
                        <div class="h1 m-0">{{$cucian_proses}}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-xl-3">
                <div class="card card-sm">
                  <div class="card-body">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <span class="bg-lime text-white avatar">
	                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="6" cy="19" r="2" /><path d="M17 17a2 2 0 1 0 2 2" /><path d="M17 17h-11v-11" /><path d="M9.239 5.231l10.761 .769l-1 7h-2m-4 0h-7" /><path d="M3 3l18 18" /></svg>
                        </span>
                      </div>
                      <div class="col">
                        <div class="font-weight-medium">
                          Cucian Selesai
                        </div>
                        <div class="h1 m-0">{{$cucian_selesai}}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-lg-12">
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Data laundry</h3>
                  </div>
                  <form>
                    <div class="card-body border-bottom py-3">
                      <div class="d-flex">
                      <div class="ms-auto text-muted">
                          <div class="ms-2 d-inline-block">
                            <select class="form-select" name="type">
                              <option value="1" {{ request()->get('type') === '1' ? 'selected' : '' }}>Kode Transaksi</option>
                              <option value="2" {{ request()->get('type') === '2' ? 'selected' : '' }}>Nama</option>
                              <option value="3" {{ request()->get('type') === '3' ? 'selected' : '' }}>No. Handphone</option>
                            </select>
                          </div>
                          <div class="ms-2 d-inline-block">
                            <input type="text" class="form-control" aria-label="Search invoice" name="query" placeholder="Search" value=" {{ request()->get('query') }}"> 
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  <div class="card-table table-responsive">
                    <table class="table card-table table-vcenter text-nowrap datatable">
                      <thead>
                        <tr>
                          <th>Tgl. Transaksi</th>
                          <th>Kode Transaksi</th>
                          <th>Nama Pelanggan</th>
                          <th>Harga</th>
                          <th>Status Pembayaran</th>
                          <th>Status Cucian</th>
                          <th></th>
                        </tr>
                      </thead>
                      @if ($data_order->count() <= 0)
                      <tr>
                        <td colspan="7">
                          Data tidak ditemukan dengan kata kunci
                          @if( request()->get('type') == '1')
                              <b>Kode Transaksi</b>
                          @elseif( request()->get('type') == '2')
                              <b>Nama</b>
                          @elseif( request()->get('type') == '3')
                              <b>Nomor Telpon</b>
                          @endif
                          <b> : {{ request()->get('query') }} </b>
                        </td>
                      </tr>
                      @endif
                      @foreach ($data_order as $order)
                        <tr>
                          <td>
                            <?php
                              $date=date_create($order->tgl_transaksi);
                              echo date_format($date,"d/m/Y H:i:s");
                            ?>
                          </td>
                          <td><a href="#" data-id="{{ $order->id}}" id="detailOId" >{{$order->id_transaksi}}</a>
                          <div class="text-muted">{{Mapping::mapping_estimasi($order->type_jasa)}}</div>
                          </td>
                          <td>{{$order->nama_costumer}}</td>
                          <td>
                            <div>Rp {{number_format($order->harga)}}</div>
                            
                          </td>
                          <td class="w-20">
                            <select class="form-select {{ Mapping::mapping_pembayaran($order->status_pembayaran)[1] }}" id="sp_{{$order->id}}" onchange="updateSP({{$order->id}});">
                              <option value="0" {{$order->status_pembayaran == '0' ? 'selected' : '' }}>Belum dibayar</option>
                              <option value="1" {{$order->status_pembayaran == '1' ? 'selected' : '' }}>Lunas</option>
                            </select>
                           </td>
                          <!-- <td id="sc_{{$order->id}}"><span class="badge {{Mapping::mapping_status_cucian($order->status_cucian)[1]}}">{{Mapping::mapping_status_cucian($order->status_cucian)[0]}}</span></td> -->
                          <td>
                            <select  class="form-select" id="spc_{{$order->id}}" onchange="updateSPC({{$order->id}});">
                              @foreach ($progress_cucian as $progress)
                                <option value="{{$progress->status}}" {{$order->status_cucian == $progress->status ? 'selected' : '' }}>{{$progress->status_desc}}</option>
                              @endforeach
                            </select>
                          </td>
                          <td class="text-end">
                            <!-- <a href="" class="btn btn-primary">Detail</a> -->
                            <div class="form-selectgroup">
                            <a href="{{url('area/admin/struk').'/'.$order->id_transaksi.'/'.$order->nama_costumer}}" target="_blank">
                              <label class="form-selectgroup-item">
                                <span class="form-selectgroup-label">
                                  <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M17 17h2a2 2 0 0 0 2 -2v-4a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h2" /><path d="M17 9v-4a2 2 0 0 0 -2 -2h-6a2 2 0 0 0 -2 2v4" /><rect x="7" y="13" width="10" height="8" rx="2" /></svg>
                                </span>
                              </label>
                              </a>
                              <a href="#" data-id="{{ $order->id}}" id="detailO">
                              <label class="form-selectgroup-item">
                                <span class="form-selectgroup-label">
                                  Detail
                                </span>
                              </label>
                              </a>
                            </div>
                          </td>
                        </tr>
                      @endforeach
                    </table>
                  </div><br>
						          {{ $data_order->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- modal -->
        @include('admin.modal_detail')
				<!-- end modal -->
        @include('admin.modal')
        <footer class="footer footer-transparent d-print-none">
          <div class="container">
            <div class="row text-center align-items-center flex-row-reverse">
              <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                <ul class="list-inline list-inline-dots mb-0">
                  <li class="list-inline-item">
                    Copyright &copy; 2021
                    <a href="." class="link-secondary">LaundyKu</a>.
                    All rights reserved.
                  </li>
                  <li class="list-inline-item">
                    <a href="./changelog.html" class="link-secondary" rel="noopener">v0.0.1 Dev</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- Libs JS -->
    <script src="{{asset('dist/libs/apexcharts/dist/apexcharts.min.js')}}"></script>
    <!-- Tabler Core -->
    <script src="{{asset('dist/js/tabler.min.js')}}"></script>
	 <script>
    <?php 
      if(Session::has('flash_message')){
    ?>
        $(window).on('load', function() {
            $('#modal-success-add').modal('show');
        });
    <?php 
    }
    ?>
	 	function updateStatus() {
			var status_js = $('select[name=status_js] option').filter(':selected').val()
			var pembayaran_js = $('#pembayaran_js').val();
			var id = $('#post_id').val();
			var token = $('input[name="_token"]').val();
			let _url     = "{{ url('area/admin/update_status') }}";
      if(status_js == "" && pembayaran_js == ""){
          $('#modal_detail').modal('hide');
          $("#title-failed").html("Infomasi");
          $("#message-failed").html("Status Wajib dipilih salah satu. Jika tidak silakan pilih cancel");
          $('#modal-danger').modal('show');
          return false;
      }
				$.ajax({
				url: _url,
				type: "POST",
				data: {
					status_cucian: status_js,
					status_pembayaran: pembayaran_js,
					id: id,
					_token: token
				},
				success: function(response) {
						if(response.status == "00") {
							$('#modal_detail').modal('hide');
              $("#message-success").html(response.message);
              $('#modal-success').modal('show');
						}else{
              $('#modal_detail').modal('hide');
              $("#message-failed").html(response.message);
              $("#title-failed").html(response.title);
              $('#modal-danger').modal('show');
            }
				},
				error: function(response) {
          $('#modal_detail').modal('hide');
          $("#message-failed").html(response);
					('#modal-danger').modal('show');
				}
				});
		}

    function updateSP(sel)
    {
      if (confirm('Apakah anda yakin ingin update status Pembayaran ini?')) {
          let _url     = "{{ url('area/admin/update_payment') }}";
          var sp_id = $('#sp_'+sel).val();
          // alert(sp_id);
          // alert(sel);
          $.ajax({
          url: _url,
          type: "POST",
          data: {
            status_pembayaran: sp_id,
            id: sel,
            _token: "{{ csrf_token() }}"
          },
          success: function(response) {
              if(response.status == "00") {
                $('#modal_detail').modal('hide');
                $("#message-success").html(response.message);
                $("#title-success").html(response.title);
                $('#modal-success').modal('show');
              }else{
                $('#modal_detail').modal('hide');
                $("#message-failed").html(response.message);
                $("#title-failed").html(response.title);
                $('#modal-danger').modal('show');
              }
          },
          error: function(response) {
            $('#modal_detail').modal('hide');
            $("#message-failed").html(response);
            ('#modal-danger').modal('show');
          }
          });
      }
    }

    function updateSPC(sel)
    {
      if (confirm('Apakah anda yakin ingin update status Progress Cucian ini?')) {
          let _url     = "{{ url('area/admin/update_progress') }}";
          var sp_id = $('#spc_'+sel).val();
          // alert(sp_id);
          // alert(sel);
          $.ajax({
          url: _url,
          type: "POST",
          data: {
            status_cucian: sp_id,
            id: sel,
            _token: "{{ csrf_token() }}"
          },
          success: function(response) {
              if(response.status == "00") {
                $('#modal_detail').modal('hide');
                $("#message-success").html(response.message);
                $('#modal-success').modal('show');
              }else{
                $('#modal_detail').modal('hide');
                $("#message-failed").html(response.message);
                $("#title-failed").html(response.title);
                $('#modal-danger').modal('show');
              }
          },
          error: function(response) {
            $('#modal_detail').modal('hide');
            $("#message-failed").html(response);
            ('#modal-danger').modal('show');
          }
          });
      }
    }
	 </script>
  </body>
</html>