<?php

namespace App\Http\Controllers\master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblAdminModel;
use App\Libraries\Mapping;
use App\Libraries\Generate;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
     /**
     * method untuk hapus petugas by id dan username
     * @return View
     */
	function index(Request $request){
        $get_user = TblAdminModel::where('level','PT')->orderBy('id','ASC')->paginate(10);
        $data_to_user = array(
            'data_user'     => $get_user,
            'active_master' =>  "active",
        );
        return View('admin.user')->with($data_to_user);
    }

    function act_add_user(Request $request) {
        $rules = array(
			'username'      => 'required',
			'name'          => 'required',
			'phone_number'  => 'required|numeric',
		);    
		$messages = array(
			'username.required'     => 'Username wajib diisi',
			'name.required'         => 'Nama Petugas wajib diisi',
			'phone_number.required' => 'Nomor Handphone wajib diisi',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
		    Session::flash('flash_message', $validator->errors()->first());
			Session::flash('flash_title', 'Ups gagal tambah Petugas');
            Session::flash('alert', 'alert-danger');
		    return Redirect::back()->withInput();
		}
        
        $add_user = TblAdminModel::create([
            'username'      => $request->input('username'),
            'password'      => Generate::password($request->input('username'),""),
            'name'          => $request->input('name'),
            'phone_number'  => $request->input('phone_number'),
            'level'         => "PT",
        ]);

        if ($add_user) {
            Session::flash('flash_message', "Tambah ".$request->input('username')." berhasil");
            Session::flash('flash_title', 'berhasil');
            Session::flash('alert', 'alert-success');
            return Redirect('area/admin/petugas')->withInput();
        }else{
            Session::flash('flash_message', "Tambah ".$request->input('username')." gagal");
            Session::flash('flash_title', 'Gagal');
            Session::flash('alert', 'alert-danger');
            return Redirect('area/admin/petugas')->withInput();
        }
    }

    /**
     * method untuk hapus petugas by id
     * @return JSON mixed
     */
    function delete(Request $request){
        $rules = array(
			'id' => 'required',
		);    
		$messages = array(
			'id.required'       => 'id User wajib diisi',
		);

        // validasi inputan
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
            return response()->json([
                'status' => "DNF",
                'message' => $validator->errors()->first(),
              ]);
		}
        //cek petugas by id
        $get_user = TblAdminModel::where('id',$request->input('id'))->first();
        if($get_user == null){
            // return jika data petugas tidak ditemukan
            return response()->json([
                  'status' => "DNF",
                  'message' => 'Data yang akan dihapus tidak ditemukan'
                ]);
        }

        //delete petugas
        $delete_user = TblAdminModel::where('id', $request->input('id'))->delete();
        if(!$delete_user) {
            // return gagal jika delete tidak berhasil
          return response()->json([
              'status' => "DUF",
              'message' => '<b>'.$get_user->username.'</b> gagal dihapus'
            ]);
        }
        // return success
        return response()->json([
          'status' => "00",
          'message' => '<b>'.$get_user->username.'</b> berhasil dihapus'
        ]);
	}

    /**
     * method untuk get data petugas by ID
     * @return JSON mixed
     */
    function get(Request $request){
        $rules = array(
			'username' => 'required',
		);    
		$messages = array(
			'username.required' => 'username wajib diisi',
		);

		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
            return response()->json([
                'status' => "DNF",
                'message' => $validator->errors()->first(),
              ]);
		}
        $get_user = TblAdminModel::where('username',$request->input('username'))->first();
        if($get_user == null) {
            return response()->json([
                  'status' => "DNF",
                  'message' => 'Data yang tidak ditemukan'
                ]);
        }
        // return success with data user
        return response()->json([
          'status' => "00",
          'message' => 'sukses',
          'id' => $get_user->id,
          'username' => $get_user->username,
          'name' => $get_user->name,
          'phone' => $get_user->phone_number,
          'level' => $get_user->level,
        ]);
	}

    /**
     * method untuk update data petugas
     * @return JSON mixed
     */
    function update_user(Request $request){
        $rules = array(
			'username'  => 'required',
			'name' => 'required',
			'phone' => 'required|numeric',
			'password' => '',
		);    
		$messages = array(
			'username.required' => 'username wajib diisi',
			'name.required'     => 'nama petugas wajib diisi',
			'phone.required'    => 'Nomor handphone wajib diisi',
		);

        //validasi inputan
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
            // return gagal jika fail
            return response()->json([
                'status' => "DNF",
                'message' => $validator->errors()->first(),
              ]);
		}

        // update user
        if($request->input('password') == '') {
            // jika password tidak diubah
            $update_user = TblAdminModel::where('username', $request->input('username'))->update([
                'name'          => $request->input('name'),
                'phone_number'  => $request->input('phone'),
            ]);
        }else{
             // jika password diubah
            $update_user = TblAdminModel::where('username', $request->input('username'))->update([
                'name'          => $request->input('name'),
                'phone_number'  => $request->input('phone'),
                'password'      => Generate::password($request->input('username'),$request->input('password')),
            ]);
        }
    
        // return jika update petugas gagal
        if(!$update_user) {
            return response()->json([
                'status' => "UDF",
                'message' => '<b>'.$request->input('username')."</b> gagal diupdate",
                ]);
        }

        //return success
        return response()->json([
          'status' => "00",
          'message' => 'Data berhasil diupdate',
        ]);
	}
}
