<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblTrxModel;
use App\Models\TblLaundrySatuanModel;
use Illuminate\Support\Facades\Log;

class DeleteOrderanController extends Controller
{
	function delete(Request $request){
        $get_orderan = TblTrxModel::where('id',$request->input('id'))->where('id_transaksi',$request->input('id_transaksi'))->first();
        if($get_orderan == null){
            return response()->json([
                  'status' => "01",
                  'message' => 'Data yang akan dihapus tidak ditemukan'
                ]);
        }
        //delete transaksi
        $delete_orderan = TblTrxModel::where('id', $request->input('id'))->delete();
        if(!$delete_orderan) {
          return response()->json([
              'status' => "02",
              'message' => 'Data gagal dihapus'
            ]);
        }
        //delete laundry satuan by id transaksi
        $delete_satuan = TblLaundrySatuanModel::where('id_transaksi',$request->input('id_transaksi'))->delete();
        return response()->json([
          'status' => "00",
          'message' => 'Data dengan kode transaksi <b>'.$get_orderan->id_transaksi.'</b> atas nama <b>'.$get_orderan->nama_costumer.'</b> berhasil dihapus'
        ]);
	}
}
