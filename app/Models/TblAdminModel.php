<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TblAdminModel extends Model {
    protected $table = 'tbl_admin';


    protected $fillable = ['username','password','name','phone_number','last_login','level'];

    public $timestamps = false;

    public $incrementing = false;
    
} 