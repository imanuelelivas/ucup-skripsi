<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblTrxModel;
use App\Models\TblJenisJasaModel;
use App\Models\TblJenisCucianModel;
use App\Models\TblStatusModel;
use App\Models\TblLaundrySatuanModel;
use App\Libraries\Mapping;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
  function home(Request $request){
    if($request->get("query") != "" && $request->get("type") == "1") {
      $dataTrx = TblTrxModel::where('id_transaksi',$request->get("query"))->where('status_cucian','!=','5')->orderBy('tgl_transaksi','DESC')->paginate(10);
    }else if($request->get("query") != "" && $request->get("type") == "2") {
      $dataTrx = TblTrxModel::where('nama_costumer','like','%'.$request->get("query").'%')->where('status_cucian','!=','5')->orderBy('tgl_transaksi','DESC')->paginate(10);
    }else if($request->get("query") != "" && $request->get("type") == "3") {
      $dataTrx = TblTrxModel::where('nomor_tlp',$request->get("query"))->where('status_cucian','!=','5')->orderBy('tgl_transaksi','DESC')->paginate(10);
    }else{
      $dataTrx = TblTrxModel::where('status_cucian','!=','5')->orderBy('tgl_transaksi','DESC')->paginate(10);
    }
    $count_cucian_masuk = TblTrxModel::whereDate('tgl_transaksi',date('Y-m-d'))->count();
    $count_cucian_antri = TblTrxModel::whereIn('status_cucian',['1','2'])->count();
    $count_cucian_proses = TblTrxModel::where('status_cucian','3')->count();
    $count_cucian_selesai = TblTrxModel::where('status_cucian','4')->count();
    $data = array(
      'active'          => "active",
      'data_order'      => $dataTrx,
      'progress_cucian' => TblStatusModel::where('group','cucian')->where('status','!=','5')->get(),
      'cucian_masuk'    => $count_cucian_masuk,
      'cucian_antri'    => $count_cucian_antri,
      'cucian_proses'   => $count_cucian_proses,
      'cucian_selesai'  => $count_cucian_selesai,
    );
    return View('admin.home')->with($data);
  }

  function detail($id){
    $get_orderan = TblTrxModel::where('id',$id)->first();
    if($get_orderan == null){
        return response()->json([
              'status' => "0",
              'message' => 'data tidak ditemukan'
            ]);
    }
    $jenis = Mapping::mapping_jenis_cucian($get_orderan->jenis_cucian);
    $jasa = Mapping::mapping_estimasi($get_orderan->type_jasa);
    $status_cucian = Mapping::mapping_status_cucian($get_orderan->status_cucian);
    $status_pembayaran = Mapping::mapping_pembayaran($get_orderan->status_pembayaran);
    $get_status_cucian = TblStatusModel::where('group','cucian')->get();
    $data =[];
    foreach ($get_status_cucian as $key => $value) {
      $data[$key]['text'] = $value->status_desc;
      $data[$key]['value'] = $value->status;
    }
    $get_laundy_satuan = TblLaundrySatuanModel::where('id_transaksi',$get_orderan->id_transaksi)->get();
    $dts_laudry =[];
    foreach ($get_laundy_satuan as $key => $value) {
      $dts_laudry[$key]['id'] = $value->id;
      $dts_laudry[$key]['id_transaksi'] = $value->id_transaksi;
      $dts_laudry[$key]['item'] = $value->item;
      $dts_laudry[$key]['qty'] = $value->qty;
      $dts_laudry[$key]['total'] = $value->total;
    }
    return response()->json([
      'status' => 1,
      'id_data' => $get_orderan->id,
      'id_transaksi' => $get_orderan->id_transaksi,
      'petugas' => $get_orderan->petugas,
      'nama_costumer' => $get_orderan->nama_costumer,
      'nomor_tlp' => $get_orderan->nomor_tlp,
      'alamat' => $get_orderan->alamat,
      'jenis_cucian' => $jenis,
      'type_jasa' => $jasa,
      'berat_cucian' => $get_orderan->berat_cucian,
      'tgl_transaksi' => $get_orderan->tgl_transaksi,
      'tgl_selesai' => $get_orderan->tgl_selesai,
      'tgl_diambil' => $get_orderan->tgl_diambil,
      'harga' => $get_orderan->harga,
      'status_cucian' => $status_cucian[0],
      'status_pembayaran' => $status_pembayaran[0],
      'tgl_update' => $get_orderan->tgl_update,
      'update_by' => $get_orderan->update_by,
      'datas' => $data,
      'dts_laudry' => $dts_laudry,
    ]);
}
}
