<table class="table card-table table-vcenter">
  <thead>
    <tr>
      <td colspan="6"  style="font-size: 12px;text-align: center;font-weight: bold;">Laporan Tahunan LaundryKu {{$year}}</td>
    </tr>
      <tr>
        <th>Tgl. Transaksi</th>
        <th>Kode Transaksi</th>
        <th>Nama Pelanggan</th>
        <th>Pembayaran</th>
        <th>Denda</th>
        <th>Harga</th>
        <th></th>
      </tr>
  </thead>
  @if ($data_harian->count() <= 0)
    <tr>
      <td colspan="7">data tidak ditemukan</td>
    </tr>
  @endif
  @foreach ($data_harian as $order)
      <tr>
        <td>{{$order->tgl_transaksi}}</td>
        <td>{{$order->id_transaksi}}</td>
        <td>{{$order->nama_costumer}}</td>
        <td>
        @if($order->status_pembayaran == '1') 
          Lunas
        @else
          Belum dibayar
        @endif
        </td>
        <td>{{number_format($order->fee)}}</td>
        <td>
          <div>{{number_format($order->harga)}}
          </div>
        </td>
      </tr>
    @endforeach
    <tr>
      <td colspan="5" class="strong text-end">Pemasukan</td>
      <td class="text-end">{{number_format($pemasukan)}}</td>
    </tr>
    <tr>
      <td colspan="5" class="strong text-end">Denda</td>
      <td class="text-end">{{number_format($fee)}}</td>
    </tr>
    <tr>
      <td colspan="5" class="strong text-end">Total Pemasukan</td>
      <td class="text-end">Rp {{number_format($pemasukan+$fee)}}</td>
    </tr>
</table>