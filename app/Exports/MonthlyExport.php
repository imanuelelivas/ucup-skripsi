<?php

namespace App\Exports;

use App\Models\TblTrxModel;
use App\Libraries\Mapping;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class MonthlyExport implements FromView
{
    public function view(): View
    {
        $date_start = request()->input('year').'-'.request()->input('month') .'-01';
        $date_end = request()->input('year').'-'.request()->input('month') .'-31';
        if(request()->input('month') != "" && request()->input('year') != "") {
            $dataTrx = TblTrxModel::whereDate('tgl_transaksi','>=',$date_start)->whereDate('tgl_transaksi','<=',$date_end);
        }else{
            $dataTrx = TblTrxModel::whereDate('tgl_transaksi','>=',date('Y').'-'.date('m').'-01')->whereDate('tgl_transaksi','<=',date('Y').'-'.date('m').'-31');
        }
        $pemasukan = $dataTrx->sum('harga');
        $fee = $dataTrx->sum('fee');
        return view('admin.report.report_monthly_excel', [
            'data_harian' => $dataTrx->get(),
            'pemasukan' => $pemasukan,
            'fee' => $fee,
            'month' => request()->input('month'),
            'year' => request()->input('year'),
        ]);
    }
}