<!-- modal -->
<div class="modal modal-blur fade" id="modal_progress" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="title">Detail Cucian</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>

         <div class="modal-body">
            <div class="row g-3">
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Kode Cucian</small>
                        <h4 id="code"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Nama Pelanggan</small>
                        <h4 id="name"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Nomor Telepon</small>
                        <h4 id="phone"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Alamat Pelanggan</small>
                        <h4 id="address"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Jenis Cucian</small>
                        <h4 id="type_laundry"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Paket Layanan</small>
                        <h4 id="estimated"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-12" id="divSatuan">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <div class="col">
                        <small class="text-muted text-truncate mt-n1">Data Laundry Satuan</small>
                           <table class="table card-table table-vcenter" id="dts_laundry">
                              <thead>
                                 <tr>
                                    <th>items</th>
                                 </tr>
                              </thead>
                              <tbody>
                              
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>   
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Berat Cucian</small>
                        <h4 id="weight"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Harga</small>
                        <h4 id="price"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Cucian diserahkan</small>
                        <h4 id="trx_date"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Tanggal cucian selesai</small>
                        <h4 id="trx_date_done"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Status Cucian</small>
                        <h4 id="status_laundry"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Status Pembayaran</small>
                        <h4 id="status_payment"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Terakhir Diupdate</small>
                        <h4 id="update_date"></h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
               Tutup
            </a>
         </div>
      </div>
   </div>
</div>
<!-- end modal -->


<!-- modal gagal -->
<div class="modal modal-blur fade" id="modal-danger" tabindex="-1" style="display: none;" aria-hidden="true">
   <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
         <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         <div class="modal-status bg-yellow"></div>
         <div class="modal-body text-center py-4">
            <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
            <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-yellow icon-lg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
               <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
               <path d="M12 9v2m0 4v.01"></path>
               <path d="M5 19h14a2 2 0 0 0 1.84 -2.75l-7.1 -12.25a2 2 0 0 0 -3.5 0l-7.1 12.25a2 2 0 0 0 1.75 2.75"></path>
            </svg>
            <h3 id="title-failed">Gagal</h3>
            <div class="text-muted" id="message-failed">gagal</div>
         </div>
         <div class="modal-footer">
            <div class="w-100">
               <div class="row">
                  <div class="col">
                     <a href="#" class="btn btn-yellow w-100" id="ok-failed" data-bs-dismiss="modal">
                        Ok
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
