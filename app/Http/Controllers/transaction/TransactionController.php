<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblTrxModel;
use App\Models\TblJenisJasaModel;
use App\Models\TblJenisCucianModel;
use App\Models\TblStatusModel;
use App\Models\TblLaundrySatuanModel;
use App\Libraries\Mapping;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{
	function index(Request $request){
    if($request->get("query") != "" && $request->get("type") == "1") {
      $dataTrx = TblTrxModel::where('id_transaksi',$request->get("query"))->where('status_cucian','4')->orderBy('tgl_transaksi','DESC')->paginate(10);
    }else if($request->get("query") != "" && $request->get("type") == "2") {
      $dataTrx = TblTrxModel::where('nama_costumer','like','%'.$request->get("query").'%')->where('status_cucian','4')->orderBy('tgl_transaksi','DESC')->paginate(10);
    }else if($request->get("query") != "" && $request->get("type") == "3") {
      $dataTrx = TblTrxModel::where('nomor_tlp',$request->get("query"))->where('status_cucian','4')->orderBy('tgl_transaksi','DESC')->paginate(10);
    }else{
      $dataTrx = TblTrxModel::orderBy('tgl_transaksi','DESC')->where('status_cucian','4')->paginate(10);
    }
    $data = array(
      'active_trx'      =>  "active",
      'data_order'      =>  $dataTrx,
      'progress_cucian'      =>  TblStatusModel::where('group','cucian')->get(),
    );
    return View('admin.transaction')->with($data);
  }

  function done_laundry(Request $request){
    if($request->get("query") != "" && $request->get("type") == "1") {
      $dataTrx = TblTrxModel::where('id_transaksi',$request->get("query"))->where('status_cucian','5')->orderBy('tgl_diambil','DESC')->paginate(10);
    }else if($request->get("query") != "" && $request->get("type") == "2") {
      $dataTrx = TblTrxModel::where('nama_costumer','like','%'.$request->get("query").'%')->where('status_cucian','5')->orderBy('tgl_diambil','DESC')->paginate(10);
    }else if($request->get("query") != "" && $request->get("type") == "3") {
      $dataTrx = TblTrxModel::where('nomor_tlp',$request->get("query"))->where('status_cucian','5')->orderBy('tgl_diambil','DESC')->paginate(10);
    }else{
      $dataTrx = TblTrxModel::orderBy('tgl_diambil','DESC')->where('status_cucian','5')->paginate(10);
    }
    $data = array(
      'active_trx'      =>  "active",
      'data_order'      =>  $dataTrx,
    );
    return View('admin.done_laundry')->with($data);
  }

  function bayar_fee(Request $request){
    $rules = array(
			'id'       	=> 'required',
			'telat' 	=> 'required|numeric',
		);    
		$messages = array(
			'id.required'       	=> 'id wajib diisi',
			'telat.required'    	=> 'telat wajib diisi',
		);
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
      return response()->json([
        'status' => "01",
        'title' => "Gagal",
        'message' => $validator->errors()->first()
      ]);
		}
    $update_orderan = false;
    $get_orderan = TblTrxModel::where('id',$request->input('id'))->first();
    if($get_orderan == null){
        return response()->json([
          'status' => "01",
          'title' => "Gagal",
          'message' => 'data tidak ditemukan'
        ]);
    }
    $telat = $request->input('telat');
    $fee = $telat * 2000;
    $status_pembayaran = $get_orderan->status_pembayaran;
    if($status_pembayaran == '1'){
      $update_orderan = TblTrxModel::where('id', $request->input('id'))->update([
        'fee'         => $fee,
        'fee_status'  => '1',
        'tgl_update'  => date('Y-m-d H:i:s'),
        'update_by'   => Session::get('nama'),
      ]);
    }else{
      $update_orderan = TblTrxModel::where('id', $request->input('id'))->update([
        'fee'         => $fee,
        'fee_status'  => '1',
        'status_pembayaran'  => '1',
        'tgl_pembayaran'  => date('Y-m-d H:i:s'),
        'tgl_update'  => date('Y-m-d H:i:s'),
        'update_by'   => Session::get('nama'),
      ]);
    }
    if(!$update_orderan) {
      return response()->json([
          'status' => "01",
          'title' => "Gagal",
          'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> Gagal Bayar Denda",
        ]);
    }

    return response()->json([
      'status' => "00",
      'title' => "Sukses",
      'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> Berhasil bayar denda",
    ]);
  }

  function ambil_cucian(Request $request){
    $rules = array(
			'id'       	=> 'required',
		);    
		$messages = array(
			'id.required'   => 'id wajib diisi',
		);
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
      return response()->json([
        'status' => "01",
        'title' => "Gagal",
        'message' => $validator->errors()->first()
      ]);
		}
    $update_orderan = false;
    $get_orderan = TblTrxModel::where('id',$request->input('id'))->first();
    if($get_orderan == null){
        return response()->json([
          'status' => "01",
          'title' => "Gagal",
          'message' => 'data tidak ditemukan'
        ]);
    }
    $status = $get_orderan->status_cucian;
    $status_pembayaran = $get_orderan->status_pembayaran;
    if($status != '4' && $status_pembayaran != '1'){
      return response()->json([
        'status' => "ACG",
        'title' => "Gagal",
        'message' => 'Ambil Cucian Gagal. Pastikan cucian sudah lunas dan cucian statusnya Selesai'
      ]);
    }
    $update_orderan = TblTrxModel::where('id', $request->input('id'))->update([
      'status_cucian'         => '5',
      'tgl_diambil'  => date('Y-m-d H:i:s'),
      'tgl_update'  => date('Y-m-d H:i:s'),
      'update_by'   => Session::get('nama'),
    ]);

    if(!$update_orderan) {
      return response()->json([
          'status' => "01",
          'title' => "Gagal",
          'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> Gagal Diambil",
        ]);
    }

    return response()->json([
      'status' => "00",
      'title' => "Sukses",
      'kode' => $get_orderan->id_transaksi,
      'name' => $get_orderan->nama_costumer,
      'message' => 'Cucian dengan Kode <b>'.$get_orderan->id_transaksi."</b> atas nama <b>".$get_orderan->nama_costumer."</b> Berhasil diambil",
    ]);
  }
}
