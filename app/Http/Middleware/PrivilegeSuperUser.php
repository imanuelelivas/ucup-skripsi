<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Redirect;
use Illuminate\Support\Facades\Log;
class PrivilegeSuperUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('LEVEL_USER')){
            if(Session::get('LEVEL_USER') == "SU"){
                return $next($request);
            }
        }
        return Redirect('404'); 
    }
}
