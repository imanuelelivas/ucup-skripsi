<?php

namespace App\Http\Controllers\transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use Excel;
use App\Models\TblTrxModel;
use App\Exports\AnnualExport;
use App\Libraries\Mapping;
use Illuminate\Support\Facades\Log;

class ReportAnnualController extends Controller
{
	function index(Request $request){
        if($request->get("year") != "") {
          $date_start = $request->get("year").'-01=01';
          $date_end = $request->get("year").'-12-31';
          $dataTrx = TblTrxModel::whereDate('tgl_transaksi','>=',$date_start)->whereDate('tgl_transaksi','<=',$date_end);
          $pemasukan = $dataTrx->sum('harga');
          $fee = $dataTrx->sum('fee');
          $report = $dataTrx->orderBy('tgl_transaksi','ASC')->paginate(10);
        $data = array(
            'active_laporan'    => "active",
            'data_harian'       => $report,
            'pemasukan'         => $pemasukan,
            'fee'               => $fee,
            'year'               => $request->get("year"),
            );
            return View('admin.report.report_annual')->with($data);
          }else{
            $data = array(
              'active_laporan'    => "active",
              'data_harian'       => "empty",
              'pemasukan'         => '',
              'fee'               => '',
              'start'               => '',
              'end'               => '',
              );
              return View('admin.report.report_annual')->with($data);
          }
          
	}

  public function exportExcel(Request $request) {
    $nama_file = 'laporan_tahunan_laundryku_'.$request->get("year").'.xlsx';
    return Excel::download(new AnnualExport, $nama_file);
  }

}
