<!-- modal -->
<div class="modal modal-blur fade" id="modal_detail" tabindex="-1" role="dialog" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="title">Detail Cucian</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
         </div>

         <div class="modal-body">
            <div class="row g-3">
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Kode Cucian</small>
                        <h4 id="code"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Nama Pelanggan</small>
                        <h4 id="name"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Nomor Telepon</small>
                        <h4 id="phone"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Alamat Pelanggan</small>
                        <h4 id="address"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Jenis Cucian</small>
                        <h4 id="type_laundry"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Estimasi Pengerjaan</small>
                        <h4 id="estimated"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-12" id="divSatuan">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <div>
                        <small class="text-muted text-truncate mt-n1">Data Laundry Satuan</small>
                           <table class="table card-table table-vcenter" id="dts_laundry">
                              <thead>
                                 <tr>
                                    <th>items</th>
                                    <th>qty</th>
                                    <th>total</th>
                                 </tr>
                              </thead>
                              <tbody>
                              
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Berat Cucian</small>
                        <h4 id="weight"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Harga</small>
                        <h4 id="price"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Tanggal Cucian Diterima</small>
                        <h4 id="trx_date"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Cucian Diterima Oleh</small>
                        <h4 id="created_by"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Cucian selesai</small>
                        <h4 id="trx_done"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Status Cucian</small>
                        <h4 id="status_laundry"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Status Pembayaran</small>
                        <h4 id="status_payment"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Terakhir Diupdate</small>
                        <h4 id="update_date"></h4>
                     </div>
                  </div>
               </div>
               <div class="col-6">
                  <div class="row g-3 align-items-center">
                     <div class="col text-truncate">
                        <small class="text-muted text-truncate mt-n1">Updated By</small>
                        <h4 id="update_by"></h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-body">
            <form>
            <input type="hidden" value="" name="post_id" id="post_id">
            <input type="hidden" value="" name="post_id_trx" id="post_id_trx">
               @csrf
            </form>
         </div>
         <div class="modal-footer">
            <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
               Cancel
            </a>
            <div class="ms-auto">
               @if (Session::has('LEVEL_USER'))
                  @if(Session::get('LEVEL_USER') == "SU")
                     <a href="#" class="btn btn btn-red btn-sm" id="hapus-data">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><line x1="4" y1="7" x2="20" y2="7" /><line x1="10" y1="11" x2="10" y2="17" /><line x1="14" y1="11" x2="14" y2="17" /><path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" /><path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /></svg>Hapus
                     </a>  
                     <a href="" class="btn btn btn-orange btn-sm" id="edit_data">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M9 7h-3a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-3" /><path d="M9 15h3l8.5 -8.5a1.5 1.5 0 0 0 -3 -3l-8.5 8.5v3" /><line x1="16" y1="5" x2="19" y2="8" /></svg>Edit
                     </a>
                  @endif
               @endif
               <a href="#" class="btn btn btn-yellow btn-sm" id="print_struk">
	               <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M17 17h2a2 2 0 0 0 2 -2v-4a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h2" /><path d="M17 9v-4a2 2 0 0 0 -2 -2h-6a2 2 0 0 0 -2 2v4" /><rect x="7" y="13" width="10" height="8" rx="2" /></svg> Struk
               </a>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- end modal -->
<script>
   $("body").on("click", "#detailO,#detailOId", function (event) {
      event.preventDefault();
      var id = $(this).data("id");
      let _url = "{{ url('area/admin/detail') }}" + "/" + id;
      $.ajax({
         url: _url,
         type: "GET",
         success: function (response) {
            if (response.status == 1) {
               $('#post_id').val(response.id_data);
               $('#post_id_trx').val(response.id_transaksi);
               $("#title").html("Detail Cucian " + response.id_transaksi);
               $("#code").html(response.id_transaksi);
               $("#name").html(response.nama_costumer);
               $("#phone").html(response.nomor_tlp);
               $("#address").html(response.alamat);
               $("#type_laundry").html(response.jenis_cucian);
               $("#estimated").html(response.type_jasa);
               if(response.berat_cucian != null){
                  $("#weight").html(response.berat_cucian+" Kg");
               }else{
                  $("#weight").html("-");
               }
               $("#price").html("Rp "+formatNumber(response.harga));
               $("#trx_date").html(response.tgl_transaksi);
               $("#trx_done").html(response.tgl_selesai);
               $("#status_laundry").html(response.status_cucian);
               $("#status_payment").html(response.status_pembayaran);
               $("#update_date").html(response.tgl_update);
               $("#update_by").html(response.update_by);
               $("#created_by").html(response.petugas);
               $("#info_status_c").html(response.status_cucian);
               $("#info_status_p").html(response.status_pembayaran);
               var urlStruk = "{{url('area/admin/struk')}}";
               $('#print_struk').attr("href", urlStruk+"/"+response.id_transaksi+"/"+response.nama_costumer);
               $('#print_struk').attr("target", "_blank");
               var urlEdit = "{{url('area/admin/edit')}}";
               $('#edit_data').attr("href", urlEdit+"/"+response.id_transaksi);
               var newOptions = response.datas;
               var newRow = response.dts_laudry;
               $("#divSatuan").show();
               var $element = $("#status_js");
               $("#dts_laundry > tbody").empty(); 
               $element.empty();
               $element.append($("<option></option>").attr("value", "").text("Pilih status Cucian"));
               $.each(newRow, function (key, value) {
                  $("#dts_laundry > tbody").append("<tr><td>"+value.item+"</td><td>"+value.qty+"</td><td>"+value.total+"</td></tr>");
               });
               if (newRow.length === 0) { 
                  $("#divSatuan").hide();
               }
               $.each(newOptions, function (key, value) {
                  $element.append($("<option></option>").attr("value", value.value).text(value.text));
               });
               $("#modal_detail").modal("show");
            } else {
               alert("gagal");
            }
         },
         error: function (response) {
            alert(response);
         },
      });
   });

   $("body").on("click", "#hapus-data", function (event) {
      if (confirm('Apakah anda yakin ingin hapus data ini?')) {
         event.preventDefault();
         var token = $('input[name="_token"]').val();
         var id = $('#post_id').val();
         var id_trx = $('#post_id_trx').val();
         let _url = "{{ url('area/admin/delete') }}";
         $.ajax({
            url: _url,
            type: "POST",
            data: {
                  id_transaksi: id_trx,
                  id: id,
                  _token: token
               },
            success: function (response) {
               if (response.status == "00") {
                  $('#modal_detail').modal('hide');
                  $("#message-success").html(response.message);
                  $('#modal-success').modal('show');
               } else {
                  alert(response.message)
               }
            },
            error: function (response) {
               alert(response);
            },
         });
      }
   });

   function formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
</script>
