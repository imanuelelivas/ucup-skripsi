<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Models\TblAdminModel;
use App\Libraries\Generate;
use Illuminate\Support\Facades\Log;

class SignInController extends Controller
{
	 /**
     * method untuk login
     * @return View
     */
    function signIn(Request $request){
        $rules = array(
			'username_int' => 'required',
			'password_int' => 'required',
		);    
		$messages = array(
			'username_int.required' => 'Username diperlukan untuk login',
			'password_int.required' => 'Password diperlukan untuk login',
		);
		// validasi inputan
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
		  	Session::flash('flash_message', $validator->errors()->first());
			Session::flash('flash_title', 'Ups Gagal Login');
		  	return Redirect::back()->withInput();
		}
		// decra variabel username & password
        $username = $request->input('username_int');
        $password = Generate::password($username,$request->input('password_int'));
		// check data user by username dan password
        $get_user = TblAdminModel::where('username',$username)->where('password',$password)->first();
		if($get_user == null){
			// return jika tidak sesuai/ditemukan
			Session::flash('flash_message', "Username/Password Salah");
			Session::flash('flash_title', 'Ups Gagal Login');
			return Redirect::back()->withInput();
		}
		// set session jika data ditemukan
		Session::put('username', $get_user->username);
		Session::put('no_telpon', $get_user->phone_number);
		Session::put('nama', $get_user->name);
		Session::put('IS_LOGIN', '1');
		Session::put('LEVEL_USER', $get_user->level);
		// redirect ke halaman utamas
		return Redirect('area/admin/home');   
    }

	 /**
     * method untuk logout
     * @return View
     */
	function signOut(Request $request){
		// delete session jika logout
		Session::forget('username');
		Session::forget('no_telpon');
		Session::forget('nama');
		Session::forget('IS_LOGIN');
		Session::flash('flash_message', "Anda Berhasil Log Out");
		Session::flash('flash_title', 'Log Out');
		// redirect ke halaman login
		return Redirect('login')->withInput();;  
	}
	
	 /**
     * method untuk ganti password
     * @return Json Mixed
     */
	function password_update(Request $request){
        $rules = array(
			'pass_old' => 'required',
			'pass_new' => 'required',
		);    
		$messages = array(
			'pass_old.required' => 'Password lama wajib untuk ganti password',
			'pass_new.required' => 'Password baru wajib untuk ganti password',
		);
		//validasi inputan
		$validator = \Validator::make( $request->all(), $rules, $messages);
		if ($validator->fails()) {
			// return gagal jika fail validasi
		  	Session::flash('flash_message', $validator->errors()->first());
			Session::flash('flash_title', 'Ups gagal ganti password');
			Session::flash('alert', 'alert-danger');
		  	return Redirect::back()->withInput();
		}
		// cek password
		// password harus berisi huruf kapital,numeric,dan huruf kecil,
		// minimal panjang 8 karakter
		$pass_new = $request->input('pass_new');
		$check_uppercase = preg_match('@[A-Z]@', $pass_new);
		$check_lowercase = preg_match('@[a-z]@', $pass_new);
		$check_number    = preg_match('@[0-9]@', $pass_new);

		if(!$check_uppercase || !$check_lowercase || !$check_number || strlen($pass_new) < 8){
			// return jika true
			Session::flash('flash_message', "Password harus minimal 8 karakter, mengandung huruf BESAR, huruf kecil dan angka");
			Session::flash('flash_title', 'Ups gagal ganti password');
			Session::flash('alert', 'alert-danger');
		  	return Redirect::back()->withInput();
		}

        $username = Session::get('username');
        $password = Generate::password($username,$request->input('pass_old'));

		// check username/password
        $get_user = TblAdminModel::where('username',$username)->where('password',$password)->first();
		if($get_user == null){
			// return jika tidak ditemukan
			Log::info("data tidak ditemukan");
			Session::flash('flash_message', "Username/Password Salah");
			Session::flash('flash_title', 'Ups gagal ganti password');
			Session::flash('alert', 'alert-danger');
			return Redirect::back()->withInput();
		}

		// proses update password ke DB
		$ganti_pass = TblAdminModel::where('username', $username)->update([
			'password'  =>  Generate::password($username,$pass_new),
		  ]);
		if(!$ganti_pass) {
			// return gagal jika update password tidak berhasil
			Log::info("gagal update password");
			Session::flash('flash_message', "Username/Password Salah");
			Session::flash('flash_title', 'Ups gagal ganti password');
			Session::flash('alert', 'alert-danger');
			return Redirect::back()->withInput();
		}

		// success
		// delete session dan login ulang jika password berhasil diubah
		Session::forget('username');
		Session::forget('no_telpon');
		Session::forget('nama');
		Session::forget('IS_LOGIN');
		Session::flash('flash_title', 'Ganti Passwor berhasil');
		Session::flash('flash_message', "Silakan Login ulang");
		Session::flash('alert', "alert-success");
		// return to page login
		return Redirect('login')->withInput();  
    }
}
